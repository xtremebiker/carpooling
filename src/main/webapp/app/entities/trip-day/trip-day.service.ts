import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITripDay } from 'app/shared/model/trip-day.model';
import { ITripPassenger } from 'app/shared/model/trip-passenger.model';

type EntityResponseType = HttpResponse<ITripDay>;
type EntityArrayResponseType = HttpResponse<ITripDay[]>;

@Injectable({ providedIn: 'root' })
export class TripDayService {
  public resourceUrl = SERVER_API_URL + 'api/trip-days';

  constructor(protected http: HttpClient) {}

  create(tripDay: ITripDay): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripDay);
    return this.http
      .post<ITripDay>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tripDay: ITripDay): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripDay);
    return this.http
      .put<ITripDay>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITripDay>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findTripDayPassengers(id: number): Observable<HttpResponse<ITripPassenger[]>> {
    return this.http
      .get<ITripPassenger[]>(`${this.resourceUrl}/${id}/passengers`, { observe: 'response' })
      .pipe(map((res: HttpResponse<ITripPassenger[]>) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITripDay[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(tripDay: ITripDay): ITripDay {
    const copy: ITripDay = Object.assign({}, tripDay, {
      tripDate: tripDay.tripDate != null && tripDay.tripDate.isValid() ? tripDay.tripDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.tripDate = res.body.tripDate != null ? moment(res.body.tripDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tripDay: ITripDay) => {
        tripDay.tripDate = tripDay.tripDate != null ? moment(tripDay.tripDate) : null;
      });
    }
    return res;
  }
}
