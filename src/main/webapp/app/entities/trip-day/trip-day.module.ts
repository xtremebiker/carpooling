import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CarpoolingSharedModule } from 'app/shared';
import {
  TripDayComponent,
  TripDayDetailComponent,
  TripDayUpdateComponent,
  TripDayDeletePopupComponent,
  TripDayDeleteDialogComponent,
  tripDayRoute,
  tripDayPopupRoute
} from './';

const ENTITY_STATES = [...tripDayRoute, ...tripDayPopupRoute];

@NgModule({
  imports: [CarpoolingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TripDayComponent,
    TripDayDetailComponent,
    TripDayUpdateComponent,
    TripDayDeleteDialogComponent,
    TripDayDeletePopupComponent
  ],
  entryComponents: [TripDayComponent, TripDayUpdateComponent, TripDayDeleteDialogComponent, TripDayDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingTripDayModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
