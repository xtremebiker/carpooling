import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ITripDay, TripDay } from 'app/shared/model/trip-day.model';
import { TripDayService } from './trip-day.service';
import { ITripGroup } from 'app/shared/model/trip-group.model';
import { TripGroupService } from 'app/entities/trip-group';

@Component({
  selector: 'jhi-trip-day-update',
  templateUrl: './trip-day-update.component.html'
})
export class TripDayUpdateComponent implements OnInit {
  tripDay: ITripDay;
  isSaving: boolean;

  tripgroups: ITripGroup[];

  editForm = this.fb.group({
    id: [],
    tripDate: [null, [Validators.required]],
    tripGroup: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected tripDayService: TripDayService,
    protected tripGroupService: TripGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tripDay }) => {
      this.updateForm(tripDay);
      this.tripDay = tripDay;
    });
    this.tripGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITripGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITripGroup[]>) => response.body)
      )
      .subscribe((res: ITripGroup[]) => (this.tripgroups = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(tripDay: ITripDay) {
    this.editForm.patchValue({
      id: tripDay.id,
      tripDate: tripDay.tripDate != null ? tripDay.tripDate.format(DATE_TIME_FORMAT) : null,
      tripGroup: tripDay.tripGroup
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tripDay = this.createFromForm();
    if (tripDay.id !== undefined) {
      this.subscribeToSaveResponse(this.tripDayService.update(tripDay));
    } else {
      this.subscribeToSaveResponse(this.tripDayService.create(tripDay));
    }
  }

  private createFromForm(): ITripDay {
    const entity = {
      ...new TripDay(),
      id: this.editForm.get(['id']).value,
      tripDate: this.editForm.get(['tripDate']).value != null ? moment(this.editForm.get(['tripDate']).value, DATE_TIME_FORMAT) : undefined,
      tripGroup: this.editForm.get(['tripGroup']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITripDay>>) {
    result.subscribe((res: HttpResponse<ITripDay>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTripGroupById(index: number, item: ITripGroup) {
    return item.id;
  }
}
