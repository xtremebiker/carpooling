import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITripDay } from 'app/shared/model/trip-day.model';

@Component({
  selector: 'jhi-trip-day-detail',
  templateUrl: './trip-day-detail.component.html'
})
export class TripDayDetailComponent implements OnInit {
  tripDay: ITripDay;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripDay }) => {
      this.tripDay = tripDay;
    });
  }

  previousState() {
    window.history.back();
  }
}
