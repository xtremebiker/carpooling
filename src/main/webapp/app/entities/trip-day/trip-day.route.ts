import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TripDay } from 'app/shared/model/trip-day.model';
import { TripDayService } from './trip-day.service';
import { TripDayComponent } from './trip-day.component';
import { TripDayDetailComponent } from './trip-day-detail.component';
import { TripDayUpdateComponent } from './trip-day-update.component';
import { TripDayDeletePopupComponent } from './trip-day-delete-dialog.component';
import { ITripDay } from 'app/shared/model/trip-day.model';

@Injectable({ providedIn: 'root' })
export class TripDayResolve implements Resolve<ITripDay> {
  constructor(private service: TripDayService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITripDay> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TripDay>) => response.ok),
        map((tripDay: HttpResponse<TripDay>) => tripDay.body)
      );
    }
    return of(new TripDay());
  }
}

export const tripDayRoute: Routes = [
  {
    path: '',
    component: TripDayComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TripDayDetailComponent,
    resolve: {
      tripDay: TripDayResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TripDayUpdateComponent,
    resolve: {
      tripDay: TripDayResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TripDayUpdateComponent,
    resolve: {
      tripDay: TripDayResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tripDayPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TripDayDeletePopupComponent,
    resolve: {
      tripDay: TripDayResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripDay.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
