import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITripDay } from 'app/shared/model/trip-day.model';
import { AccountService } from 'app/core';
import { TripDayService } from './trip-day.service';

@Component({
  selector: 'jhi-trip-day',
  templateUrl: './trip-day.component.html'
})
export class TripDayComponent implements OnInit, OnDestroy {
  tripDays: ITripDay[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected tripDayService: TripDayService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.tripDayService
      .query()
      .pipe(
        filter((res: HttpResponse<ITripDay[]>) => res.ok),
        map((res: HttpResponse<ITripDay[]>) => res.body)
      )
      .subscribe(
        (res: ITripDay[]) => {
          this.tripDays = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTripDays();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITripDay) {
    return item.id;
  }

  registerChangeInTripDays() {
    this.eventSubscriber = this.eventManager.subscribe('tripDayListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
