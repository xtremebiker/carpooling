import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITripDay } from 'app/shared/model/trip-day.model';
import { TripDayService } from './trip-day.service';

@Component({
  selector: 'jhi-trip-day-delete-dialog',
  templateUrl: './trip-day-delete-dialog.component.html'
})
export class TripDayDeleteDialogComponent {
  tripDay: ITripDay;

  constructor(protected tripDayService: TripDayService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.tripDayService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'tripDayListModification',
        content: 'Deleted an tripDay'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-trip-day-delete-popup',
  template: ''
})
export class TripDayDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripDay }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TripDayDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.tripDay = tripDay;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/trip-day', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/trip-day', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
