import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITripGroup } from 'app/shared/model/trip-group.model';

@Component({
  selector: 'jhi-trip-group-detail',
  templateUrl: './trip-group-detail.component.html'
})
export class TripGroupDetailComponent implements OnInit {
  tripGroup: ITripGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripGroup }) => {
      this.tripGroup = tripGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
