import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map, flatMap } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITripGroup, TripGroup } from 'app/shared/model/trip-group.model';
import { ITripDay } from 'app/shared/model/trip-day.model';

type EntityResponseType = HttpResponse<ITripGroup>;
type EntityArrayResponseType = HttpResponse<ITripGroup[]>;

@Injectable({ providedIn: 'root' })
export class TripGroupService {
  public resourceUrl = SERVER_API_URL + 'api/trip-groups';

  constructor(protected http: HttpClient) {}

  create(tripGroup: ITripGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripGroup);
    return this.http
      .post<ITripGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tripGroup: ITripGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripGroup);
    return this.http
      .put<ITripGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITripGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findFilteredTripDays(filter: string): Observable<EntityArrayResponseType> {
    return this.http
      .get<TripGroup[]>(`${this.resourceUrl}/for-user`, { observe: 'response' })
      .pipe(
        flatMap((res: EntityArrayResponseType) =>
          this.http
            .get<ITripDay[]>(this.resourceUrl + '/' + res.body[0].id + '/trip-days?filter=' + filter, { observe: 'response' })
            .pipe(map((res1: EntityArrayResponseType) => this.convertDateArrayFromServer(res1)))
        )
      );
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITripGroup[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(tripGroup: ITripGroup): ITripGroup {
    const copy: ITripGroup = Object.assign({}, tripGroup, {});
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tripGroup: ITripGroup) => {});
    }
    return res;
  }
}
