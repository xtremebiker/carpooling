import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TripGroup } from 'app/shared/model/trip-group.model';
import { TripGroupService } from './trip-group.service';
import { TripGroupComponent } from './trip-group.component';
import { TripGroupDetailComponent } from './trip-group-detail.component';
import { TripGroupUpdateComponent } from './trip-group-update.component';
import { TripGroupDeletePopupComponent } from './trip-group-delete-dialog.component';
import { ITripGroup } from 'app/shared/model/trip-group.model';

@Injectable({ providedIn: 'root' })
export class TripGroupResolve implements Resolve<ITripGroup> {
  constructor(private service: TripGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITripGroup> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TripGroup>) => response.ok),
        map((tripGroup: HttpResponse<TripGroup>) => tripGroup.body)
      );
    }
    return of(new TripGroup());
  }
}

export const tripGroupRoute: Routes = [
  {
    path: '',
    component: TripGroupComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TripGroupDetailComponent,
    resolve: {
      tripGroup: TripGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TripGroupUpdateComponent,
    resolve: {
      tripGroup: TripGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TripGroupUpdateComponent,
    resolve: {
      tripGroup: TripGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tripGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TripGroupDeletePopupComponent,
    resolve: {
      tripGroup: TripGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
