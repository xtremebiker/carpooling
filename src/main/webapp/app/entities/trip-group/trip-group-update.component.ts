import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ITripGroup, TripGroup } from 'app/shared/model/trip-group.model';
import { TripGroupService } from './trip-group.service';

@Component({
  selector: 'jhi-trip-group-update',
  templateUrl: './trip-group-update.component.html'
})
export class TripGroupUpdateComponent implements OnInit {
  tripGroup: ITripGroup;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    groupName: [null, [Validators.required]]
  });

  constructor(protected tripGroupService: TripGroupService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tripGroup }) => {
      this.updateForm(tripGroup);
      this.tripGroup = tripGroup;
    });
  }

  updateForm(tripGroup: ITripGroup) {
    this.editForm.patchValue({
      id: tripGroup.id,
      groupName: tripGroup.groupName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tripGroup = this.createFromForm();
    if (tripGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.tripGroupService.update(tripGroup));
    } else {
      this.subscribeToSaveResponse(this.tripGroupService.create(tripGroup));
    }
  }

  private createFromForm(): ITripGroup {
    const entity = {
      ...new TripGroup(),
      id: this.editForm.get(['id']).value,
      groupName: this.editForm.get(['groupName']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITripGroup>>) {
    result.subscribe((res: HttpResponse<ITripGroup>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
