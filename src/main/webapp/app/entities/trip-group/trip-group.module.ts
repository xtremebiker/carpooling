import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CarpoolingSharedModule } from 'app/shared';
import {
  TripGroupComponent,
  TripGroupDetailComponent,
  TripGroupUpdateComponent,
  TripGroupDeletePopupComponent,
  TripGroupDeleteDialogComponent,
  tripGroupRoute,
  tripGroupPopupRoute
} from './';

const ENTITY_STATES = [...tripGroupRoute, ...tripGroupPopupRoute];

@NgModule({
  imports: [CarpoolingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TripGroupComponent,
    TripGroupDetailComponent,
    TripGroupUpdateComponent,
    TripGroupDeleteDialogComponent,
    TripGroupDeletePopupComponent
  ],
  entryComponents: [TripGroupComponent, TripGroupUpdateComponent, TripGroupDeleteDialogComponent, TripGroupDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingTripGroupModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
