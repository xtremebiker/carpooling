import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITripGroup } from 'app/shared/model/trip-group.model';
import { AccountService } from 'app/core';
import { TripGroupService } from './trip-group.service';

@Component({
  selector: 'jhi-trip-group',
  templateUrl: './trip-group.component.html'
})
export class TripGroupComponent implements OnInit, OnDestroy {
  tripGroups: ITripGroup[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected tripGroupService: TripGroupService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.tripGroupService
      .query()
      .pipe(
        filter((res: HttpResponse<ITripGroup[]>) => res.ok),
        map((res: HttpResponse<ITripGroup[]>) => res.body)
      )
      .subscribe(
        (res: ITripGroup[]) => {
          this.tripGroups = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTripGroups();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITripGroup) {
    return item.id;
  }

  registerChangeInTripGroups() {
    this.eventSubscriber = this.eventManager.subscribe('tripGroupListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
