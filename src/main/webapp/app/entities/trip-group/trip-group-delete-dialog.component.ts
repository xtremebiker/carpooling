import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITripGroup } from 'app/shared/model/trip-group.model';
import { TripGroupService } from './trip-group.service';

@Component({
  selector: 'jhi-trip-group-delete-dialog',
  templateUrl: './trip-group-delete-dialog.component.html'
})
export class TripGroupDeleteDialogComponent {
  tripGroup: ITripGroup;

  constructor(protected tripGroupService: TripGroupService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.tripGroupService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'tripGroupListModification',
        content: 'Deleted an tripGroup'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-trip-group-delete-popup',
  template: ''
})
export class TripGroupDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripGroup }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TripGroupDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.tripGroup = tripGroup;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/trip-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/trip-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
