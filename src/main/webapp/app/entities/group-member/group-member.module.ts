import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CarpoolingSharedModule } from 'app/shared';
import {
  GroupMemberComponent,
  GroupMemberDetailComponent,
  GroupMemberUpdateComponent,
  GroupMemberDeletePopupComponent,
  GroupMemberDeleteDialogComponent,
  groupMemberRoute,
  groupMemberPopupRoute
} from './';

const ENTITY_STATES = [...groupMemberRoute, ...groupMemberPopupRoute];

@NgModule({
  imports: [CarpoolingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    GroupMemberComponent,
    GroupMemberDetailComponent,
    GroupMemberUpdateComponent,
    GroupMemberDeleteDialogComponent,
    GroupMemberDeletePopupComponent
  ],
  entryComponents: [GroupMemberComponent, GroupMemberUpdateComponent, GroupMemberDeleteDialogComponent, GroupMemberDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingGroupMemberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
