import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IGroupMember, GroupMember } from 'app/shared/model/group-member.model';
import { GroupMemberService } from './group-member.service';
import { ITripGroup } from 'app/shared/model/trip-group.model';
import { TripGroupService } from 'app/entities/trip-group';

@Component({
  selector: 'jhi-group-member-update',
  templateUrl: './group-member-update.component.html'
})
export class GroupMemberUpdateComponent implements OnInit {
  groupMember: IGroupMember;
  isSaving: boolean;

  tripgroups: ITripGroup[];

  editForm = this.fb.group({
    id: [],
    authId: [null, [Validators.required]],
    firstName: [null, [Validators.required]],
    lastName: [null, [Validators.required]],
    email: [null, [Validators.required]],
    phone: [],
    canDrive: [],
    permission: [null, [Validators.required]],
    driverRatio: [null, [Validators.required]],
    tripGroup: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected groupMemberService: GroupMemberService,
    protected tripGroupService: TripGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ groupMember }) => {
      this.updateForm(groupMember);
      this.groupMember = groupMember;
    });
    this.tripGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITripGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITripGroup[]>) => response.body)
      )
      .subscribe((res: ITripGroup[]) => (this.tripgroups = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(groupMember: IGroupMember) {
    this.editForm.patchValue({
      id: groupMember.id,
      authId: groupMember.authId,
      firstName: groupMember.firstName,
      lastName: groupMember.lastName,
      email: groupMember.email,
      phone: groupMember.phone,
      canDrive: groupMember.canDrive,
      permission: groupMember.permission,
      driverRatio: groupMember.driverRatio,
      tripGroup: groupMember.tripGroup
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const groupMember = this.createFromForm();
    if (groupMember.id !== undefined) {
      this.subscribeToSaveResponse(this.groupMemberService.update(groupMember));
    } else {
      this.subscribeToSaveResponse(this.groupMemberService.create(groupMember));
    }
  }

  private createFromForm(): IGroupMember {
    const entity = {
      ...new GroupMember(),
      id: this.editForm.get(['id']).value,
      authId: this.editForm.get(['authId']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      email: this.editForm.get(['email']).value,
      phone: this.editForm.get(['phone']).value,
      canDrive: this.editForm.get(['canDrive']).value,
      permission: this.editForm.get(['permission']).value,
      driverRatio: this.editForm.get(['driverRatio']).value,
      tripGroup: this.editForm.get(['tripGroup']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGroupMember>>) {
    result.subscribe((res: HttpResponse<IGroupMember>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTripGroupById(index: number, item: ITripGroup) {
    return item.id;
  }
}
