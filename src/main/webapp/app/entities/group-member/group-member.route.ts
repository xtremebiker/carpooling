import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GroupMember } from 'app/shared/model/group-member.model';
import { GroupMemberService } from './group-member.service';
import { GroupMemberComponent } from './group-member.component';
import { GroupMemberDetailComponent } from './group-member-detail.component';
import { GroupMemberUpdateComponent } from './group-member-update.component';
import { GroupMemberDeletePopupComponent } from './group-member-delete-dialog.component';
import { IGroupMember } from 'app/shared/model/group-member.model';

@Injectable({ providedIn: 'root' })
export class GroupMemberResolve implements Resolve<IGroupMember> {
  constructor(private service: GroupMemberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IGroupMember> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<GroupMember>) => response.ok),
        map((groupMember: HttpResponse<GroupMember>) => groupMember.body)
      );
    }
    return of(new GroupMember());
  }
}

export const groupMemberRoute: Routes = [
  {
    path: '',
    component: GroupMemberComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.groupMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: GroupMemberDetailComponent,
    resolve: {
      groupMember: GroupMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.groupMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: GroupMemberUpdateComponent,
    resolve: {
      groupMember: GroupMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.groupMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: GroupMemberUpdateComponent,
    resolve: {
      groupMember: GroupMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.groupMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const groupMemberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: GroupMemberDeletePopupComponent,
    resolve: {
      groupMember: GroupMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.groupMember.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
