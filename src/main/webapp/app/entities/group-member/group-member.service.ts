import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IGroupMember } from 'app/shared/model/group-member.model';

type EntityResponseType = HttpResponse<IGroupMember>;
type EntityArrayResponseType = HttpResponse<IGroupMember[]>;

@Injectable({ providedIn: 'root' })
export class GroupMemberService {
  public resourceUrl = SERVER_API_URL + 'api/group-members';

  constructor(protected http: HttpClient) {}

  create(groupMember: IGroupMember): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(groupMember);
    return this.http
      .post<IGroupMember>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(groupMember: IGroupMember): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(groupMember);
    return this.http
      .put<IGroupMember>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGroupMember>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGroupMember[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(groupMember: IGroupMember): IGroupMember {
    const copy: IGroupMember = Object.assign({}, groupMember, {});
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((groupMember: IGroupMember) => {});
    }
    return res;
  }
}
