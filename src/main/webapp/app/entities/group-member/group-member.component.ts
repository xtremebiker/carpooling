import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IGroupMember } from 'app/shared/model/group-member.model';
import { AccountService } from 'app/core';
import { GroupMemberService } from './group-member.service';

@Component({
  selector: 'jhi-group-member',
  templateUrl: './group-member.component.html'
})
export class GroupMemberComponent implements OnInit, OnDestroy {
  groupMembers: IGroupMember[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected groupMemberService: GroupMemberService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.groupMemberService
      .query()
      .pipe(
        filter((res: HttpResponse<IGroupMember[]>) => res.ok),
        map((res: HttpResponse<IGroupMember[]>) => res.body)
      )
      .subscribe(
        (res: IGroupMember[]) => {
          this.groupMembers = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInGroupMembers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IGroupMember) {
    return item.id;
  }

  registerChangeInGroupMembers() {
    this.eventSubscriber = this.eventManager.subscribe('groupMemberListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
