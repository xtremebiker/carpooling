import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IGroupMember } from 'app/shared/model/group-member.model';
import { GroupMemberService } from './group-member.service';

@Component({
  selector: 'jhi-group-member-delete-dialog',
  templateUrl: './group-member-delete-dialog.component.html'
})
export class GroupMemberDeleteDialogComponent {
  groupMember: IGroupMember;

  constructor(
    protected groupMemberService: GroupMemberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.groupMemberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'groupMemberListModification',
        content: 'Deleted an groupMember'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-group-member-delete-popup',
  template: ''
})
export class GroupMemberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ groupMember }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(GroupMemberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.groupMember = groupMember;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/group-member', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/group-member', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
