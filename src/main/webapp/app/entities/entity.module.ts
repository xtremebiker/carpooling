import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'trip-group',
        loadChildren: './trip-group/trip-group.module#CarpoolingTripGroupModule'
      },
      {
        path: 'group-member',
        loadChildren: './group-member/group-member.module#CarpoolingGroupMemberModule'
      },
      {
        path: 'trip-day',
        loadChildren: './trip-day/trip-day.module#CarpoolingTripDayModule'
      },
      {
        path: 'trip-passenger',
        loadChildren: './trip-passenger/trip-passenger.module#CarpoolingTripPassengerModule'
      },
      {
        path: 'group-member',
        loadChildren: './group-member/group-member.module#CarpoolingGroupMemberModule'
      },
      {
        path: 'trip-passenger',
        loadChildren: './trip-passenger/trip-passenger.module#CarpoolingTripPassengerModule'
      },
      {
        path: 'group-member',
        loadChildren: './group-member/group-member.module#CarpoolingGroupMemberModule'
      },
      {
        path: 'trip-day',
        loadChildren: './trip-day/trip-day.module#CarpoolingTripDayModule'
      },
      {
        path: 'trip-passenger',
        loadChildren: './trip-passenger/trip-passenger.module#CarpoolingTripPassengerModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingEntityModule {}
