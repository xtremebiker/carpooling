import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITripPassenger } from 'app/shared/model/trip-passenger.model';
import { JhiAlertService } from 'ng-jhipster';

type EntityResponseType = HttpResponse<ITripPassenger>;
type EntityArrayResponseType = HttpResponse<ITripPassenger[]>;

@Injectable({ providedIn: 'root' })
export class TripPassengerService {
  public resourceUrl = SERVER_API_URL + 'api/trip-passengers';

  constructor(protected http: HttpClient) {}

  create(tripPassenger: ITripPassenger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripPassenger);
    return this.http
      .post<ITripPassenger>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tripPassenger: ITripPassenger): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tripPassenger);
    return this.http
      .put<ITripPassenger>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITripPassenger>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITripPassenger[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(tripPassenger: ITripPassenger): ITripPassenger {
    const copy: ITripPassenger = Object.assign({}, tripPassenger, {});
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tripPassenger: ITripPassenger) => {});
    }
    return res;
  }
}
