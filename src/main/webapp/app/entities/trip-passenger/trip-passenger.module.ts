import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { CarpoolingSharedModule } from 'app/shared';
import {
  TripPassengerComponent,
  TripPassengerDetailComponent,
  TripPassengerUpdateComponent,
  TripPassengerDeletePopupComponent,
  TripPassengerDeleteDialogComponent,
  tripPassengerRoute,
  tripPassengerPopupRoute
} from './';

const ENTITY_STATES = [...tripPassengerRoute, ...tripPassengerPopupRoute];

@NgModule({
  imports: [CarpoolingSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TripPassengerComponent,
    TripPassengerDetailComponent,
    TripPassengerUpdateComponent,
    TripPassengerDeleteDialogComponent,
    TripPassengerDeletePopupComponent
  ],
  entryComponents: [
    TripPassengerComponent,
    TripPassengerUpdateComponent,
    TripPassengerDeleteDialogComponent,
    TripPassengerDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingTripPassengerModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
