import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TripPassenger } from 'app/shared/model/trip-passenger.model';
import { TripPassengerService } from './trip-passenger.service';
import { TripPassengerComponent } from './trip-passenger.component';
import { TripPassengerDetailComponent } from './trip-passenger-detail.component';
import { TripPassengerUpdateComponent } from './trip-passenger-update.component';
import { TripPassengerDeletePopupComponent } from './trip-passenger-delete-dialog.component';
import { ITripPassenger } from 'app/shared/model/trip-passenger.model';

@Injectable({ providedIn: 'root' })
export class TripPassengerResolve implements Resolve<ITripPassenger> {
  constructor(private service: TripPassengerService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITripPassenger> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TripPassenger>) => response.ok),
        map((tripPassenger: HttpResponse<TripPassenger>) => tripPassenger.body)
      );
    }
    return of(new TripPassenger());
  }
}

export const tripPassengerRoute: Routes = [
  {
    path: '',
    component: TripPassengerComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripPassenger.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TripPassengerDetailComponent,
    resolve: {
      tripPassenger: TripPassengerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripPassenger.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TripPassengerUpdateComponent,
    resolve: {
      tripPassenger: TripPassengerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripPassenger.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TripPassengerUpdateComponent,
    resolve: {
      tripPassenger: TripPassengerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripPassenger.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tripPassengerPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TripPassengerDeletePopupComponent,
    resolve: {
      tripPassenger: TripPassengerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'carpoolingApp.tripPassenger.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
