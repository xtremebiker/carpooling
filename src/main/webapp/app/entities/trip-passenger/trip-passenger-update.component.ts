import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ITripPassenger, TripPassenger } from 'app/shared/model/trip-passenger.model';
import { TripPassengerService } from './trip-passenger.service';
import { IGroupMember } from 'app/shared/model/group-member.model';
import { GroupMemberService } from 'app/entities/group-member';
import { ITripDay } from 'app/shared/model/trip-day.model';
import { TripDayService } from 'app/entities/trip-day';

@Component({
  selector: 'jhi-trip-passenger-update',
  templateUrl: './trip-passenger-update.component.html'
})
export class TripPassengerUpdateComponent implements OnInit {
  tripPassenger: ITripPassenger;
  isSaving: boolean;

  groupmembers: IGroupMember[];

  tripdays: ITripDay[];

  editForm = this.fb.group({
    id: [],
    passengerStatus: [null, [Validators.required]],
    ratio: [null, [Validators.required]],
    canDrive: [],
    groupMember: [],
    tripDay: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected tripPassengerService: TripPassengerService,
    protected groupMemberService: GroupMemberService,
    protected tripDayService: TripDayService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tripPassenger }) => {
      this.updateForm(tripPassenger);
      this.tripPassenger = tripPassenger;
    });
    this.groupMemberService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGroupMember[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGroupMember[]>) => response.body)
      )
      .subscribe((res: IGroupMember[]) => (this.groupmembers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.tripDayService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITripDay[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITripDay[]>) => response.body)
      )
      .subscribe((res: ITripDay[]) => (this.tripdays = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(tripPassenger: ITripPassenger) {
    this.editForm.patchValue({
      id: tripPassenger.id,
      passengerStatus: tripPassenger.passengerStatus,
      ratio: tripPassenger.ratio,
      canDrive: tripPassenger.canDrive,
      groupMember: tripPassenger.groupMember,
      tripDay: tripPassenger.tripDay
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tripPassenger = this.createFromForm();
    if (tripPassenger.id !== undefined) {
      this.subscribeToSaveResponse(this.tripPassengerService.update(tripPassenger));
    } else {
      this.subscribeToSaveResponse(this.tripPassengerService.create(tripPassenger));
    }
  }

  private createFromForm(): ITripPassenger {
    const entity = {
      ...new TripPassenger(),
      id: this.editForm.get(['id']).value,
      passengerStatus: this.editForm.get(['passengerStatus']).value,
      ratio: this.editForm.get(['ratio']).value,
      canDrive: this.editForm.get(['canDrive']).value,
      groupMember: this.editForm.get(['groupMember']).value,
      tripDay: this.editForm.get(['tripDay']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITripPassenger>>) {
    result.subscribe((res: HttpResponse<ITripPassenger>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGroupMemberById(index: number, item: IGroupMember) {
    return item.id;
  }

  trackTripDayById(index: number, item: ITripDay) {
    return item.id;
  }
}
