import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITripPassenger } from 'app/shared/model/trip-passenger.model';
import { AccountService } from 'app/core';
import { TripPassengerService } from './trip-passenger.service';

@Component({
  selector: 'jhi-trip-passenger',
  templateUrl: './trip-passenger.component.html'
})
export class TripPassengerComponent implements OnInit, OnDestroy {
  tripPassengers: ITripPassenger[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected tripPassengerService: TripPassengerService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.tripPassengerService
      .query()
      .pipe(
        filter((res: HttpResponse<ITripPassenger[]>) => res.ok),
        map((res: HttpResponse<ITripPassenger[]>) => res.body)
      )
      .subscribe(
        (res: ITripPassenger[]) => {
          this.tripPassengers = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTripPassengers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITripPassenger) {
    return item.id;
  }

  registerChangeInTripPassengers() {
    this.eventSubscriber = this.eventManager.subscribe('tripPassengerListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
