import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITripPassenger } from 'app/shared/model/trip-passenger.model';

@Component({
  selector: 'jhi-trip-passenger-detail',
  templateUrl: './trip-passenger-detail.component.html'
})
export class TripPassengerDetailComponent implements OnInit {
  tripPassenger: ITripPassenger;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripPassenger }) => {
      this.tripPassenger = tripPassenger;
    });
  }

  previousState() {
    window.history.back();
  }
}
