export * from './trip-passenger.service';
export * from './trip-passenger-update.component';
export * from './trip-passenger-delete-dialog.component';
export * from './trip-passenger-detail.component';
export * from './trip-passenger.component';
export * from './trip-passenger.route';
