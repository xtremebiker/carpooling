import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITripPassenger } from 'app/shared/model/trip-passenger.model';
import { TripPassengerService } from './trip-passenger.service';

@Component({
  selector: 'jhi-trip-passenger-delete-dialog',
  templateUrl: './trip-passenger-delete-dialog.component.html'
})
export class TripPassengerDeleteDialogComponent {
  tripPassenger: ITripPassenger;

  constructor(
    protected tripPassengerService: TripPassengerService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.tripPassengerService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'tripPassengerListModification',
        content: 'Deleted an tripPassenger'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-trip-passenger-delete-popup',
  template: ''
})
export class TripPassengerDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tripPassenger }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TripPassengerDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.tripPassenger = tripPassenger;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/trip-passenger', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/trip-passenger', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
