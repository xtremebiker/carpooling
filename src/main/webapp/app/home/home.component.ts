import { Component, OnInit } from '@angular/core';

import { LoginService, AccountService, Account } from 'app/core';
import { TripGroupService } from 'app/entities/trip-group';
import { ITripDay, TripDay } from 'app/shared/model/trip-day.model';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { filter, map, flatMap } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager, JhiTruncateCharactersPipe } from 'ng-jhipster';
import { Subscription, Observable, forkJoin, zip } from 'rxjs';
import { ITripPassenger, TripPassenger, PassengerStatus } from 'app/shared/model/trip-passenger.model';
import { TripDayService } from 'app/entities/trip-day';
import { TripPassengerService } from 'app/entities/trip-passenger';
import { GroupMember } from 'app/shared/model/group-member.model';
import { GroupMemberService } from 'app/entities/group-member';
import * as moment from 'moment';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
  account: Account;
  nextTripDays: ITripDay[];
  nextTripDayPassengers: Map<number, ITripPassenger[]>;
  nextTripDaySelectableMembers: Map<number, GroupMember[]>;
  nextTripDayDriverSuggestion: Map<number, ITripPassenger>;
  eventSubscriber: Subscription;
  selectedMember: any = null;
  loading = false;
  /**
   * Tells whether to show the current and next trip days (NEXT) or the days before (PREVIOUS)
   */
  tripDays: string = null;
  memberSorting = (m1: GroupMember, m2: GroupMember) => {
    return m1.firstName.localeCompare(m2.firstName);
  };
  passengerSorting = (p1: TripPassenger, p2: TripPassenger) => {
    return this.memberSorting(p1.groupMember, p2.groupMember);
  };

  constructor(
    private accountService: AccountService,
    private loginService: LoginService,
    protected groupMemberService: GroupMemberService,
    protected tripGroupService: TripGroupService,
    protected tripDayService: TripDayService,
    protected tripPassengerService: TripPassengerService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.registerChangeInTripDays();
    // Subscribe to parameter change
    // https://stackoverflow.com/a/59256242/1199132
    this.route.queryParams.subscribe(result => {
      this.tripDays = result['tripDays'];
      zip(
        this.accountService.fetch().pipe(
          filter((res: HttpResponse<Account>) => res.ok),
          map((res: HttpResponse<Account>) => res.body)
        ),
        this.accountService.identity()
      ).subscribe(responses => {
        this.loadAll();
        this.account = responses[1];
      });
    });
  }

  registerChangeInTripDays() {
    this.eventSubscriber = this.eventManager.subscribe('tripDayListModification', response => this.loadAll());
  }

  trackId(index: number, item: ITripDay) {
    return item.id;
  }

  /**
   * Adds all the selectable members from a trip day as passengers
   * @param tripDay
   */
  addAllAsPassengers(tripDay: TripDay) {
    const clonedArray = this.nextTripDaySelectableMembers.get(tripDay.id).map(e => ({ ...e }));
    clonedArray.forEach(member => this.addMemberAsPassenger(tripDay, member.id));
  }

  /**
   * Switches the trip passenger status to DRIVES or PASSENGER
   * @param passenger
   */
  switchDrives(passenger: TripPassenger) {
    let alertType;
    if (passenger.passengerStatus === PassengerStatus.DRIVES) {
      passenger.passengerStatus = PassengerStatus.PASSENGER;
      passenger.ratio = 1;
      alertType = 'carpoolingApp.tripPassenger.doesntDrive';
    } else {
      passenger.passengerStatus = PassengerStatus.DRIVES;
      passenger.ratio = 1;
      alertType = 'carpoolingApp.tripPassenger.drives';
    }
    this.tripPassengerService
      .update(passenger)
      .pipe(
        filter((res: HttpResponse<TripPassenger>) => res.ok),
        map((res: HttpResponse<TripPassenger>) => res.body)
      )
      .subscribe((res: TripPassenger) => {
        this.updatePassengerRatio(passenger.groupMember.id, res.groupMember.driverRatio);
        this.jhiAlertService.success(alertType, { param: passenger.groupMember.firstName });
      });
  }

  /**
   * Updates the member ratio for all the passengers that belong to a group member that has been altered
   * @param memberId
   * @param ratio
   */
  private updatePassengerRatio(memberId: number, ratio: number) {
    this.nextTripDayPassengers.forEach((value: ITripPassenger[], key: number) => {
      value.forEach((val: ITripPassenger) => {
        if (val.groupMember.id === memberId) {
          val.groupMember.driverRatio = ratio;
        }
      });
    });
    // Only suggest a driver when showing the next trip days, not the previous ones
    if (this.showTripDays() === 'NEXT') {
      this.updateDriverSuggestions();
    }
  }

  /**
   * Updates the passenger suggestion to drive for each of the trip days
   */
  private updateDriverSuggestions() {
    this.nextTripDayDriverSuggestion.clear();
    this.nextTripDayPassengers.forEach((value: ITripPassenger[], key: number) => {
      // The 'lastDroveUpdated' property stores the last drove date considering the current modifications in the view (still not loaded from DB)
      value.forEach((val: ITripPassenger) => (val.groupMember['lastDroveUpdated'] = val.groupMember.lastDrove));
      value.forEach((val: ITripPassenger) => {
        // If the last drove date for the passenger happens to be smaller than the one set in the view (not refreshed yet from DB): update
        if (val.passengerStatus === 'DRIVES' && moment(val.groupMember.lastDrove).diff(moment(val.tripDay.tripDate)) < 0) {
          val.groupMember['lastDroveUpdated'] = val.tripDay.tripDate;
        }
      });
      const sorted = value
        .map(e => ({ ...e }))
        .filter(p => p.passengerStatus !== PassengerStatus.DRIVES)
        .sort((p, p2) => {
          return (
            // Prefer lower ratio first
            p.groupMember.driverRatio - p2.groupMember.driverRatio ||
            // If ratio is the same, compare the last dates the members drove, prefer the earlier
            (p.groupMember['lastDroveUpdated'] &&
              p2.groupMember['lastDroveUpdated'] &&
              moment(p.groupMember['lastDroveUpdated']).diff(moment(p2.groupMember['lastDroveUpdated']))) ||
            // If tied, use standard passenger sorting
            this.passengerSorting(p, p2)
          );
        });
      value.forEach((val: ITripPassenger) => {
        // Only suggest a driver if the remaining passengers don't fit a car (four passengers)
        const driverNum: number = value.filter(p => p.passengerStatus === PassengerStatus.DRIVES).length;
        if ((driverNum === 0 || value.length / (driverNum * 4) > 1) && val.groupMember.id === sorted[0].groupMember.id) {
          this.nextTripDayDriverSuggestion.set(key, val.groupMember);
        }
      });
    });
  }

  /**
   * Returns whether to show the NEXT or PREVIOUS trip days based in the value
   */
  private showTripDays(): string {
    return this.tripDays != null ? this.tripDays : 'NEXT';
  }

  loadAll() {
    this.loading = true;
    this.nextTripDayPassengers = new Map();
    this.nextTripDaySelectableMembers = new Map();
    this.nextTripDayDriverSuggestion = new Map();
    this.tripGroupService
      .findFilteredTripDays(this.showTripDays())
      .pipe(
        filter((res: HttpResponse<ITripDay[]>) => res.ok),
        map((res: HttpResponse<ITripDay[]>) => res.body)
      )
      .subscribe((res: ITripDay[]) => {
        this.nextTripDays = res;
        if (this.nextTripDays.length) {
          this.groupMemberService
            .query({ idGroup: this.nextTripDays[0].tripGroup.id })
            .pipe(
              filter((response: HttpResponse<GroupMember[]>) => response.ok),
              map((resp: HttpResponse<GroupMember[]>) => resp.body),
              map((members: GroupMember[]) => members.sort(this.memberSorting)),
              map((members: GroupMember[]) => {
                const requests = [];
                this.nextTripDays.forEach((tripDay: ITripDay, index) => {
                  // Initialize the array of passengers for the trip day
                  this.nextTripDayPassengers.set(tripDay.id, []);
                  if (
                    index < this.nextTripDays.length - 1 &&
                    moment(tripDay.tripDate).isoWeek() < moment(this.nextTripDays[index + 1].tripDate).isoWeek()
                  ) {
                    tripDay['lastInWeek'] = true;
                  }
                  // Initialize the selectable members with a cloned instance of the member array
                  this.nextTripDaySelectableMembers.set(tripDay.id, members.map(e => ({ ...e })));
                  // Make an array of requests to find the trip days passengers in parallel
                  requests.push(
                    this.tripDayService.findTripDayPassengers(tripDay.id).pipe(
                      filter((response: HttpResponse<ITripPassenger[]>) => response.ok),
                      map((resp: HttpResponse<ITripPassenger[]>) => resp.body)
                    )
                  );
                });
                forkJoin(requests).subscribe((responses: ITripPassenger[][]) => {
                  responses.forEach((passengers: ITripPassenger[]) => {
                    // The 'lastDroveUpdated' property stores the last drove date considering the current modifications in the view (still not loaded from DB)
                    passengers.forEach((val: ITripPassenger) => (val.groupMember['lastDroveUpdated'] = val.groupMember.lastDrove));
                    if (passengers.length) {
                      passengers.sort(this.passengerSorting);
                      this.nextTripDayPassengers.set(passengers[0].tripDay.id, passengers);
                      this.nextTripDaySelectableMembers.set(
                        passengers[0].tripDay.id,
                        members.filter(
                          (member: GroupMember) =>
                            !this.nextTripDayPassengers
                              .get(passengers[0].tripDay.id)
                              .find(passenger => passenger.groupMember.id === member.id)
                        )
                      );
                    }
                  });
                  // Only suggest a driver when showing the next trip days, not the previous ones
                  if (this.showTripDays() === 'NEXT') {
                    this.updateDriverSuggestions();
                  }
                  this.loading = false;
                });
              })
            )
            .subscribe();
        } else {
          // There are no trip days available for this group
          this.loading = false;
        }
      });
  }

  removePassenger(tripDay: TripDay, passenger: TripPassenger) {
    this.tripPassengerService
      .delete(passenger.id)
      .pipe(filter((response: HttpResponse<GroupMember>) => response.ok))
      .subscribe(res => {
        // Remove the passenger from the trip day passengers and add it to the selectable members
        const passengers = this.nextTripDayPassengers.get(tripDay.id);
        const index = passengers.indexOf(passenger, 0);
        if (index > -1) {
          passengers.splice(index, 1);
        }
        this.groupMemberService
          .find(passenger.groupMember.id)
          .pipe(
            filter((response: HttpResponse<GroupMember>) => response.ok),
            map((resp: HttpResponse<GroupMember>) => resp.body)
          )
          .subscribe(member => {
            this.updatePassengerRatio(member.id, member.driverRatio);
            this.nextTripDaySelectableMembers.get(tripDay.id).push(member);
            this.nextTripDaySelectableMembers.get(tripDay.id).sort(this.memberSorting);
            this.selectedMember = 'null';
          });
      });
  }

  addMemberAsPassenger(tripDay, memberId: number) {
    this.groupMemberService
      .find(Number(memberId))
      .pipe(
        filter((response: HttpResponse<GroupMember>) => response.ok),
        map((resp: HttpResponse<GroupMember>) => resp.body),
        map((member: GroupMember) =>
          this.tripPassengerService
            .create({ passengerStatus: PassengerStatus.PASSENGER, ratio: 1, groupMember: member, canDrive: true, tripDay })
            .pipe(
              filter((response: HttpResponse<ITripPassenger>) => response.ok),
              map((resp: HttpResponse<ITripPassenger>) => resp.body)
            )
            .subscribe((createdPassenger: ITripPassenger) => {
              this.nextTripDayPassengers.get(tripDay.id).push(createdPassenger);
              this.nextTripDayPassengers.get(tripDay.id).sort(this.passengerSorting);
              const index = this.nextTripDaySelectableMembers.get(tripDay.id).findIndex(memb => memb.id === member.id);
              this.nextTripDaySelectableMembers.get(tripDay.id).splice(index, 1);
              this.updatePassengerRatio(createdPassenger.groupMember.id, createdPassenger.groupMember.driverRatio);
              this.selectedMember = 'null';
            })
        )
      )
      .subscribe();
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  login() {
    this.loginService.login();
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
