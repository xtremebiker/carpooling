import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CarpoolingSharedLibsModule, CarpoolingSharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CarpoolingSharedLibsModule, CarpoolingSharedCommonModule],
  declarations: [HasAnyAuthorityDirective],
  exports: [CarpoolingSharedCommonModule, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarpoolingSharedModule {
  static forRoot() {
    return {
      ngModule: CarpoolingSharedModule
    };
  }
}
