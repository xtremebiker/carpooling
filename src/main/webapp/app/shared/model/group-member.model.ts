import { Moment } from 'moment';
import { ITripGroup } from 'app/shared/model/trip-group.model';

export const enum Role {
  USER = 'USER',
  ADMIN = 'ADMIN'
}

export interface IGroupMember {
  id?: number;
  authId?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  canDrive?: boolean;
  lastDrove?: Moment;
  permission?: Role;
  driverRatio?: number;
  tripGroup?: ITripGroup;
}

export class GroupMember implements IGroupMember {
  constructor(
    public id?: number,
    public authId?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phone?: string,
    public canDrive?: boolean,
    public lastDrove?: Moment,
    public permission?: Role,
    public driverRatio?: number,
    public tripGroup?: ITripGroup
  ) {
    this.canDrive = this.canDrive || false;
  }
}
