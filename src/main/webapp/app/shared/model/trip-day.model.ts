import { Moment } from 'moment';
import { ITripPassenger } from 'app/shared/model/trip-passenger.model';
import { ITripGroup } from 'app/shared/model/trip-group.model';

export interface ITripDay {
  id?: number;
  tripDate?: Moment;
  passengers?: ITripPassenger[];
  tripGroup?: ITripGroup;
}

export class TripDay implements ITripDay {
  constructor(public id?: number, public tripDate?: Moment, public passengers?: ITripPassenger[], public tripGroup?: ITripGroup) {}
}
