import { Moment } from 'moment';
import { IGroupMember } from 'app/shared/model/group-member.model';
import { ITripDay } from 'app/shared/model/trip-day.model';

export const enum PassengerStatus {
  DRIVES = 'DRIVES',
  PASSENGER = 'PASSENGER',
  NOT_GOING = 'NOT_GOING'
}

export interface ITripPassenger {
  id?: number;
  passengerStatus?: PassengerStatus;
  ratio?: number;
  canDrive?: boolean;
  groupMember?: IGroupMember;
  tripDay?: ITripDay;
}

export class TripPassenger implements ITripPassenger {
  constructor(
    public id?: number,
    public passengerStatus?: PassengerStatus,
    public ratio?: number,
    public canDrive?: boolean,
    public created?: Moment,
    public createdBy?: string,
    public updated?: Moment,
    public updatedBy?: string,
    public active?: boolean,
    public groupMember?: IGroupMember,
    public tripDay?: ITripDay
  ) {
    this.canDrive = this.canDrive || false;
    this.active = this.active || false;
  }
}
