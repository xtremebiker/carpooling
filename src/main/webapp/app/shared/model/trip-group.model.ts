import { Moment } from 'moment';

export interface ITripGroup {
  id?: number;
  groupName?: string;
}

export class TripGroup implements ITripGroup {
  constructor(public id?: number, public groupName?: string) {}
}
