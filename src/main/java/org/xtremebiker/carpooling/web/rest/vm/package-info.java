/**
 * View Models used by Spring MVC REST controllers.
 */
package org.xtremebiker.carpooling.web.rest.vm;
