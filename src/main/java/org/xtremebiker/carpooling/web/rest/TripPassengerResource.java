package org.xtremebiker.carpooling.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.repository.GroupMemberRepository;
import org.xtremebiker.carpooling.repository.TripPassengerRepository;
import org.xtremebiker.carpooling.service.AuditHelperService;
import org.xtremebiker.carpooling.service.impl.TripDayServiceImpl;
import org.xtremebiker.carpooling.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link org.xtremebiker.carpooling.domain.TripPassenger}.
 */
@RestController
@RequestMapping("/api")
public class TripPassengerResource {

	private final Logger log = LoggerFactory.getLogger(TripPassengerResource.class);

	private static final String ENTITY_NAME = "tripPassenger";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final TripPassengerRepository tripPassengerRepository;

	private final GroupMemberRepository groupMemberRepository;

	private final TripDayServiceImpl tripDayServiceImpl;

	private final AuditHelperService auditHelperService;

	public TripPassengerResource(AuditHelperService auditHelperService, TripPassengerRepository tripPassengerRepository,
			GroupMemberRepository groupMemberRepository, TripDayServiceImpl tripDayServiceImpl) {
		this.tripPassengerRepository = tripPassengerRepository;
		this.groupMemberRepository = groupMemberRepository;
		this.auditHelperService = auditHelperService;
		this.tripDayServiceImpl = tripDayServiceImpl;
	}

	/**
	 * {@code POST  /trip-passengers} : Create a new tripPassenger.
	 *
	 * @param tripPassenger the tripPassenger to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new tripPassenger, or with status {@code 400 (Bad Request)}
	 *         if the tripPassenger has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/trip-passengers")
	public ResponseEntity<TripPassenger> createTripPassenger(@Valid @RequestBody TripPassenger tripPassenger)
			throws URISyntaxException {
		log.debug("REST request to save TripPassenger : {}", tripPassenger);
		if (tripPassenger.getId() != null) {
			throw new BadRequestAlertException("A new tripPassenger cannot already have an ID", ENTITY_NAME,
					"idexists");
		}
		auditHelperService.updateAuditFields(tripPassenger, id -> tripPassengerRepository.findByIdAndActiveTrue(id));
		TripPassenger result = tripPassengerRepository.save(tripPassenger);
		tripDayServiceImpl
				.updateRatios(this.groupMemberRepository.findByIdAndActiveTrue(result.getGroupMember().getId()).get());
		return ResponseEntity.created(new URI("/api/trip-passengers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME,
						result.getId().toString()))
				.body(tripPassengerRepository.findByIdAndActiveTrue(result.getId()).get());
	}

	/**
	 * {@code PUT  /trip-passengers} : Updates an existing tripPassenger.
	 *
	 * @param tripPassenger the tripPassenger to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated tripPassenger, or with status {@code 400 (Bad Request)}
	 *         if the tripPassenger is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the tripPassenger couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/trip-passengers")
	public ResponseEntity<TripPassenger> updateTripPassenger(@Valid @RequestBody TripPassenger tripPassenger)
			throws URISyntaxException {
		log.debug("REST request to update TripPassenger : {}", tripPassenger);
		if (tripPassenger.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		auditHelperService.updateAuditFields(tripPassenger, id -> tripPassengerRepository.findByIdAndActiveTrue(id));
		TripPassenger result = tripPassengerRepository.save(tripPassenger);
		tripDayServiceImpl
				.updateRatios(this.groupMemberRepository.findByIdAndActiveTrue(result.getGroupMember().getId()).get());
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
						tripPassenger.getId().toString()))
				.body(tripPassengerRepository.findByIdAndActiveTrue(result.getId()).get());
	}

	/**
	 * {@code GET  /trip-passengers} : get all the tripPassengers.
	 *
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of tripPassengers in body.
	 */
	@GetMapping("/trip-passengers")
	public List<TripPassenger> getAllTripPassengers() {
		log.debug("REST request to get all TripPassengers");
		return tripPassengerRepository.findAllByActiveTrue();
	}

	/**
	 * {@code GET  /trip-passengers/:id} : get the "id" tripPassenger.
	 *
	 * @param id the id of the tripPassenger to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the tripPassenger, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/trip-passengers/{id}")
	public ResponseEntity<TripPassenger> getTripPassenger(@PathVariable Long id) {
		log.debug("REST request to get TripPassenger : {}", id);
		Optional<TripPassenger> tripPassenger = tripPassengerRepository.findByIdAndActiveTrue(id);
		return ResponseUtil.wrapOrNotFound(tripPassenger);
	}

	/**
	 * {@code DELETE  /trip-passengers/:id} : delete the "id" tripPassenger.
	 *
	 * @param id the id of the tripPassenger to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/trip-passengers/{id}")
	public ResponseEntity<Void> deleteTripPassenger(@PathVariable Long id) {
		log.debug("REST request to delete TripPassenger : {}", id);
		Optional<TripPassenger> tripPassenger = tripPassengerRepository.findByIdAndActiveTrue(id);
		auditHelperService.markAsInactive(tripPassenger.get());
		tripPassengerRepository.save(tripPassenger.get());
		tripDayServiceImpl.updateRatios(
				this.groupMemberRepository.findByIdAndActiveTrue(tripPassenger.get().getGroupMember().getId()).get());
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}
}
