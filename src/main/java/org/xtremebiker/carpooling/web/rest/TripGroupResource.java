package org.xtremebiker.carpooling.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.xtremebiker.carpooling.domain.TripDay;
import org.xtremebiker.carpooling.domain.TripGroup;
import org.xtremebiker.carpooling.repository.TripDayRepository;
import org.xtremebiker.carpooling.service.TripGroupService;
import org.xtremebiker.carpooling.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link org.xtremebiker.carpooling.domain.TripGroup}.
 */
@RestController
@RequestMapping("/api")
public class TripGroupResource {

	private final Logger log = LoggerFactory.getLogger(TripGroupResource.class);

	private static final String ENTITY_NAME = "tripGroup";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final TripGroupService tripGroupService;

	private final TripDayRepository tripDayRepository;

	public TripGroupResource(TripGroupService tripGroupService, TripDayRepository tripDayRepository) {
		this.tripGroupService = tripGroupService;
		this.tripDayRepository = tripDayRepository;
	}

	/**
	 * {@code POST  /trip-groups} : Create a new tripGroup.
	 *
	 * @param tripGroup the tripGroup to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new tripGroup, or with status {@code 400 (Bad Request)} if
	 *         the tripGroup has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/trip-groups")
	public ResponseEntity<TripGroup> createTripGroup(@Valid @RequestBody TripGroup tripGroup)
			throws URISyntaxException {
		log.debug("REST request to save TripGroup : {}", tripGroup);
		if (tripGroup.getId() != null) {
			throw new BadRequestAlertException("A new tripGroup cannot already have an ID", ENTITY_NAME, "idexists");
		}
		TripGroup result = tripGroupService.save(tripGroup);
		return ResponseEntity
				.created(new URI("/api/trip-groups/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * {@code PUT  /trip-groups} : Updates an existing tripGroup.
	 *
	 * @param tripGroup the tripGroup to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated tripGroup, or with status {@code 400 (Bad Request)} if
	 *         the tripGroup is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the tripGroup couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/trip-groups")
	public ResponseEntity<TripGroup> updateTripGroup(@Valid @RequestBody TripGroup tripGroup)
			throws URISyntaxException {
		log.debug("REST request to update TripGroup : {}", tripGroup);
		if (tripGroup.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		TripGroup result = tripGroupService.save(tripGroup);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tripGroup.getId().toString()))
				.body(result);
	}

	/**
	 * {@code GET  /trip-groups} : get all the tripGroups.
	 *
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of tripGroups in body.
	 */
	@GetMapping("/trip-groups")
	public List<TripGroup> getAllTripGroups() {
		log.debug("REST request to get all TripGroups");
		return tripGroupService.findAll();
	}

	/**
	 * {@code GET  /trip-groups} : get all the User's tripGroups.
	 *
	 * @param idUser The user for which to retrieve the group, current logged user
	 *               if this is null.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of tripGroups in body.
	 */
	@GetMapping("/trip-groups/for-user")
	public List<TripGroup> getAllUserTripGroups(@RequestParam(required = false) String idAuth, Principal user) {
		if (idAuth == null) {
			idAuth = user.getName();
		}
		log.debug("REST request to get all TripGroups for the User {}", idAuth);
		return tripGroupService.findAllByUser(idAuth);
	}

	/**
	 * {@code GET  /trip-groups/:id} : get the "id" tripGroup.
	 *
	 * @param id the id of the tripGroup to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the tripGroup, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/trip-groups/{id}")
	public ResponseEntity<TripGroup> getTripGroup(@PathVariable Long id) {
		log.debug("REST request to get TripGroup : {}", id);
		Optional<TripGroup> tripGroup = tripGroupService.findOne(id);
		return ResponseUtil.wrapOrNotFound(tripGroup);
	}

	/**
	 * {@code GET  /trip-days} : get all the tripDays for the group.
	 *
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of tripDays in body.
	 */
	@GetMapping("/trip-groups/{idGroup}/trip-days")
	public List<TripDay> getAllTripDays(@PathVariable Long idGroup, @RequestParam(required = false) String filter) {
		log.debug("REST request to get all TripDays");
		if (filter == null) {
			return tripDayRepository.findAll();
		}
		ZonedDateTime now = ZonedDateTime.now();
		if (filter.equals("NEXT")) {
			int daysTillEndOfWeek = 8 - now.getDayOfWeek().getValue();
			return tripDayRepository.findByTripGroupIdAndTripDateBetweenAndActiveTrueOrderByTripDate(idGroup,
					now.truncatedTo(ChronoUnit.DAYS), now.truncatedTo(ChronoUnit.DAYS).plusDays(7 + daysTillEndOfWeek));
		}
		if (filter.equals("PREVIOUS")) {
			int daysTillBeginningOfPreviousWeek = 6 + now.getDayOfWeek().getValue();
			return tripDayRepository.findByTripGroupIdAndTripDateBetweenAndActiveTrueOrderByTripDateDesc(idGroup,
					now.truncatedTo(ChronoUnit.DAYS).minusDays(daysTillBeginningOfPreviousWeek),
					now.truncatedTo(ChronoUnit.DAYS));
		} else {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid filter value " + filter);
		}
	}

	/**
	 * {@code DELETE  /trip-groups/:id} : delete the "id" tripGroup.
	 *
	 * @param id the id of the tripGroup to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/trip-groups/{id}")
	public ResponseEntity<Void> deleteTripGroup(@PathVariable Long id) {
		log.debug("REST request to delete TripGroup : {}", id);
		tripGroupService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}
}
