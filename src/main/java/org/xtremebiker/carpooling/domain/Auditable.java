package org.xtremebiker.carpooling.domain;

import java.time.ZonedDateTime;

/**
 * Defines a set of accessors for the auditable entities
 */
public interface Auditable {

    Long getId();

    ZonedDateTime getCreated();

    void setCreated(ZonedDateTime created);

    void setUpdated(ZonedDateTime created);

    String getCreatedBy();

    void setCreatedBy(String createdBy);

    ZonedDateTime getUpdated();

    String getUpdatedBy();

    void setUpdatedBy(String updatedBy);

    Boolean isActive();

    void setActive(Boolean active);
}