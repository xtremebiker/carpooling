package org.xtremebiker.carpooling.domain;


import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.xtremebiker.carpooling.domain.enumeration.PassengerStatus;

/**
 * A TripPassenger.
 */
@Entity
@Table(name = "trip_passenger")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TripPassenger implements Serializable, Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "passenger_status", nullable = false)
    private PassengerStatus passengerStatus;

    @NotNull
    @Column(name = "ratio", nullable = false)
    private Double ratio;

    @Column(name = "can_drive")
    private Boolean canDrive;

    @JsonIgnore
    @Column(name = "created", nullable = false)
    private ZonedDateTime created;

    @JsonIgnore
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @JsonIgnore
    @Column(name = "updated")
    private ZonedDateTime updated;

    @JsonIgnore
    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnore
    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    @JsonIgnoreProperties("tripPassengers")
    private GroupMember groupMember;

    @ManyToOne
    @JsonIgnoreProperties("passengers")
    private TripDay tripDay;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PassengerStatus getPassengerStatus() {
        return passengerStatus;
    }

    public TripPassenger passengerStatus(PassengerStatus passengerStatus) {
        this.passengerStatus = passengerStatus;
        return this;
    }

    public void setPassengerStatus(PassengerStatus passengerStatus) {
        this.passengerStatus = passengerStatus;
    }

    public Double getRatio() {
        return ratio;
    }

    public TripPassenger ratio(Double ratio) {
        this.ratio = ratio;
        return this;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }

    public Boolean isCanDrive() {
        return canDrive;
    }

    public TripPassenger canDrive(Boolean canDrive) {
        this.canDrive = canDrive;
        return this;
    }

    public void setCanDrive(Boolean canDrive) {
        this.canDrive = canDrive;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public TripPassenger created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public TripPassenger createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public ZonedDateTime getUpdated() {
        return updated;
    }

    public TripPassenger updated(ZonedDateTime updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public TripPassenger updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean isActive() {
        return active;
    }

    public TripPassenger active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public GroupMember getGroupMember() {
        return groupMember;
    }

    public TripPassenger groupMember(GroupMember groupMember) {
        this.groupMember = groupMember;
        return this;
    }

    public void setGroupMember(GroupMember groupMember) {
        this.groupMember = groupMember;
    }

    public TripDay getTripDay() {
        return tripDay;
    }

    public TripPassenger tripDay(TripDay tripDay) {
        this.tripDay = tripDay;
        return this;
    }

    public void setTripDay(TripDay tripDay) {
        this.tripDay = tripDay;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TripPassenger)) {
            return false;
        }
        return id != null && id.equals(((TripPassenger) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TripPassenger{" +
            "id=" + getId() +
            ", passengerStatus='" + getPassengerStatus() + "'" +
            ", ratio=" + getRatio() +
            ", canDrive='" + isCanDrive() + "'" +
            ", created='" + getCreated() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
