package org.xtremebiker.carpooling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

import org.xtremebiker.carpooling.domain.enumeration.Role;

/**
 * A GroupMember.
 */
@Entity
@Table(name = "group_member")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GroupMember implements Serializable, Auditable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "auth_id", nullable = false)
	private String authId;

	@NotNull
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotNull
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@NotNull
	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "phone")
	private String phone;

	@Column(name = "can_drive")
	private Boolean canDrive;

	@Formula("(select max(day.trip_date) from trip_passenger passenger join trip_day day on passenger.trip_day_id = day.id where passenger.group_member_id = id and passenger.active = 1 "
			+ "and passenger.passenger_status = 'DRIVES')")
	private ZonedDateTime lastDrove;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "permission", nullable = false)
	private Role permission;

	@NotNull
	@Column(name = "driver_ratio", nullable = false)
	private Double driverRatio;

	@JsonIgnore
	@Column(name = "created", nullable = false)
	private ZonedDateTime created;

	@JsonIgnore
	@Column(name = "created_by", nullable = false)
	private String createdBy;

	@JsonIgnore
	@Column(name = "updated")
	private ZonedDateTime updated;

	@JsonIgnore
	@Column(name = "updated_by")
	private String updatedBy;

	@JsonIgnore
	@Column(name = "active")
	private Boolean active;

	@ManyToOne
	@JsonIgnoreProperties("groupMembers")
	private TripGroup tripGroup;

	public GroupMember active(Boolean active) {
		this.active = active;
		return this;
	}

	public GroupMember authId(String authId) {
		this.authId = authId;
		return this;
	}

	public GroupMember canDrive(Boolean canDrive) {
		this.canDrive = canDrive;
		return this;
	}

	public GroupMember created(ZonedDateTime created) {
		this.created = created;
		return this;
	}

	public GroupMember createdBy(String createdBy) {
		this.createdBy = createdBy;
		return this;
	}

	public GroupMember driverRatio(Double driverRatio) {
		this.driverRatio = driverRatio;
		return this;
	}

	public GroupMember email(String email) {
		this.email = email;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GroupMember)) {
			return false;
		}
		return id != null && id.equals(((GroupMember) o).id);
	}

	public GroupMember firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getAuthId() {
		return authId;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Double getDriverRatio() {
		return driverRatio;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public ZonedDateTime getLastDrove() {
		return lastDrove;
	}

	public String getLastName() {
		return lastName;
	}

	public Role getPermission() {
		return permission;
	}

	public String getPhone() {
		return phone;
	}

	public TripGroup getTripGroup() {
		return tripGroup;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	public Boolean isActive() {
		return active;
	}

	public Boolean isCanDrive() {
		return canDrive;
	}

	public GroupMember lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public GroupMember permission(Role permission) {
		this.permission = permission;
		return this;
	}

	public GroupMember phone(String phone) {
		this.phone = phone;
		return this;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setAuthId(String authId) {
		this.authId = authId;
	}

	public void setCanDrive(Boolean canDrive) {
		this.canDrive = canDrive;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setDriverRatio(Double driverRatio) {
		this.driverRatio = driverRatio;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPermission(Role permission) {
		this.permission = permission;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setTripGroup(TripGroup tripGroup) {
		this.tripGroup = tripGroup;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "GroupMember{" + "id=" + getId() + ", authId='" + getAuthId() + "'" + ", firstName='" + getFirstName()
				+ "'" + ", lastName='" + getLastName() + "'" + ", email='" + getEmail() + "'" + ", phone='" + getPhone()
				+ "'" + ", canDrive='" + isCanDrive() + "'" + ", permission='" + getPermission() + "'"
				+ ", driverRatio=" + getDriverRatio() + ", created='" + getCreated() + "'" + ", createdBy='"
				+ getCreatedBy() + "'" + ", updated='" + getUpdated() + "'" + ", updatedBy='" + getUpdatedBy() + "'"
				+ ", active='" + isActive() + "'" + "}";
	}

	public GroupMember tripGroup(TripGroup tripGroup) {
		this.tripGroup = tripGroup;
		return this;
	}

	public GroupMember updated(ZonedDateTime updated) {
		this.updated = updated;
		return this;
	}

	public GroupMember updatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
		return this;
	}
}
