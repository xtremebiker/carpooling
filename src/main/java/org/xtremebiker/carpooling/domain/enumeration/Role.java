package org.xtremebiker.carpooling.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    USER, ADMIN
}
