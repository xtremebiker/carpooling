package org.xtremebiker.carpooling.domain.enumeration;

/**
 * The PassengerStatus enumeration.
 */
public enum PassengerStatus {
    DRIVES, PASSENGER, NOT_GOING
}
