package org.xtremebiker.carpooling.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A TripDay.
 */
@Entity
@Table(name = "trip_day")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TripDay implements Serializable, Auditable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "trip_date", nullable = false)
    private ZonedDateTime tripDate;

    @JsonIgnore
    @Column(name = "created", nullable = false)
    private ZonedDateTime created;

    @JsonIgnore
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @JsonIgnore
    @Column(name = "updated")
    private ZonedDateTime updated;

    @JsonIgnore
    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnore
    @Column(name = "active")
    private Boolean active;

    @OneToMany(mappedBy = "tripDay")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TripPassenger> passengers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("tripDays")
    private TripGroup tripGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTripDate() {
        return tripDate;
    }

    public TripDay tripDate(ZonedDateTime tripDate) {
        this.tripDate = tripDate;
        return this;
    }

    public void setTripDate(ZonedDateTime tripDate) {
        this.tripDate = tripDate;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public TripDay created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public TripDay createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public ZonedDateTime getUpdated() {
        return updated;
    }

    public TripDay updated(ZonedDateTime updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public TripDay updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Boolean isActive() {
        return active;
    }

    public TripDay active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<TripPassenger> getPassengers() {
        return passengers;
    }

    public TripDay passengers(Set<TripPassenger> tripPassengers) {
        this.passengers = tripPassengers;
        return this;
    }

    public TripDay addPassenger(TripPassenger tripPassenger) {
        this.passengers.add(tripPassenger);
        tripPassenger.setTripDay(this);
        return this;
    }

    public TripDay removePassenger(TripPassenger tripPassenger) {
        this.passengers.remove(tripPassenger);
        tripPassenger.setTripDay(null);
        return this;
    }

    public void setPassengers(Set<TripPassenger> tripPassengers) {
        this.passengers = tripPassengers;
    }

    public TripGroup getTripGroup() {
        return tripGroup;
    }

    public TripDay tripGroup(TripGroup tripGroup) {
        this.tripGroup = tripGroup;
        return this;
    }

    public void setTripGroup(TripGroup tripGroup) {
        this.tripGroup = tripGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TripDay)) {
            return false;
        }
        return id != null && id.equals(((TripDay) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TripDay{" +
            "id=" + getId() +
            ", tripDate='" + getTripDate() + "'" +
            ", created='" + getCreated() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
