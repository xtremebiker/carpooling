package org.xtremebiker.carpooling.service;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.xtremebiker.carpooling.domain.GroupMember;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.service.impl.TripDayServiceImpl;

/**
 * Sends notification e-mails
 * 
 * @author aritz
 *
 */
@Service
public class MailSenderService {

	private JavaMailSender sender;

	private Logger logger = LoggerFactory.getLogger(MailSenderService.class);

	private TripDayServiceImpl tripDayServiceImpl;

	public MailSenderService(JavaMailSender sender, TripDayServiceImpl tripDayServiceImpl) {
		this.sender = sender;
		this.tripDayServiceImpl = tripDayServiceImpl;
	}

	/**
	 * Sends notification mails to the group members that are scheduled for driving
	 * the next day
	 */
	@Scheduled(cron = "0 0 17 * * ?")
	public void notifyNextDayDrivers() {
		List<TripPassenger> drivers = this.tripDayServiceImpl.driversTomorrow();
		drivers.stream().forEach(driver -> {
			GroupMember member = driver.getGroupMember();
			logger.info("Sending driver notification mail to {}", member.getEmail());
			MimeMessage msg = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, "utf-8");
			try {
				helper.setTo(member.getEmail());
				helper.setFrom("info@quienconduce.com");
				helper.setSubject("Nos vemos mañana");
				helper.setText("¡Hola " + member.getFirstName()
						+ "! Recuerda que mañana te toca conducir.<br><a href='https://www.quienconduce.com'>https://www.quienconduce.com</a>",
						true);
				sender.send(msg);
			} catch (MessagingException e) {
				// Log & continue
				logger.error(e.getMessage(), e);
			}
		});
	}
}
