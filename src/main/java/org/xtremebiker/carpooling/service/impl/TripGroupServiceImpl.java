package org.xtremebiker.carpooling.service.impl;

import org.xtremebiker.carpooling.service.AuditHelperService;
import org.xtremebiker.carpooling.service.TripGroupService;
import org.xtremebiker.carpooling.domain.GroupMember;
import org.xtremebiker.carpooling.domain.TripGroup;
import org.xtremebiker.carpooling.repository.GroupMemberRepository;
import org.xtremebiker.carpooling.repository.TripGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TripGroup}.
 */
@Service
@Transactional
public class TripGroupServiceImpl implements TripGroupService {

	private final Logger log = LoggerFactory.getLogger(TripGroupServiceImpl.class);

	private final TripGroupRepository tripGroupRepository;

	private final GroupMemberRepository groupMemberRepository;

	private final AuditHelperService auditHelperService;

	public TripGroupServiceImpl(AuditHelperService auditHelperService, TripGroupRepository tripGroupRepository,
			GroupMemberRepository groupMemberRepository) {
		this.tripGroupRepository = tripGroupRepository;
		this.groupMemberRepository = groupMemberRepository;
		this.auditHelperService = auditHelperService;
	}

	/**
	 * Save a tripGroup.
	 *
	 * @param tripGroup the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public TripGroup save(TripGroup tripGroup) {
		log.debug("Request to save TripGroup : {}", tripGroup);
		auditHelperService.updateAuditFields(tripGroup, id -> tripGroupRepository.findByIdAndActiveTrue(id));
		return tripGroupRepository.save(tripGroup);
	}

	/**
	 * Get all the tripGroups.
	 *
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<TripGroup> findAll() {
		log.debug("Request to get all TripGroups");
		return tripGroupRepository.findAllByActiveTrue();
	}

	/**
	 * Get one tripGroup by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<TripGroup> findOne(Long id) {
		log.debug("Request to get TripGroup : {}", id);
		return tripGroupRepository.findByIdAndActiveTrue(id);
	}

	/**
	 * Delete the tripGroup by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete TripGroup : {}", id);
		Optional<TripGroup> tripGroup = tripGroupRepository.findByIdAndActiveTrue(id);
		if (tripGroup.isPresent()) {
			auditHelperService.markAsInactive(tripGroup.get());
			tripGroupRepository.save(tripGroup.get());
		}
	}

	@Override
	public List<TripGroup> findAllByUser(String idAuth) {
		return this.groupMemberRepository.findByAuthIdAndActiveTrue(idAuth).stream().map(GroupMember::getTripGroup)
				.collect(Collectors.toList());
	}
}
