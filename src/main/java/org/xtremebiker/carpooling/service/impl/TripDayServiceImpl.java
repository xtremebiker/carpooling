package org.xtremebiker.carpooling.service.impl;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xtremebiker.carpooling.domain.GroupMember;
import org.xtremebiker.carpooling.domain.TripDay;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.domain.enumeration.PassengerStatus;
import org.xtremebiker.carpooling.repository.GroupMemberRepository;
import org.xtremebiker.carpooling.repository.TripDayRepository;
import org.xtremebiker.carpooling.repository.TripPassengerRepository;
import org.xtremebiker.carpooling.service.AuditHelperService;

@Service
@Transactional
public class TripDayServiceImpl {

	private final Logger log = LoggerFactory.getLogger(TripDayServiceImpl.class);

	private final TripDayRepository tripDayRepository;

	private final AuditHelperService auditHelperService;

	private final TripPassengerRepository tripPassengerRepository;

	private final GroupMemberRepository groupMemberRepository;

	public TripDayServiceImpl(AuditHelperService auditHelperService, TripDayRepository tripDayRepository,
			TripPassengerRepository tripPassengerRepository, GroupMemberRepository groupMemberRepository) {
		this.auditHelperService = auditHelperService;
		this.tripDayRepository = tripDayRepository;
		this.tripPassengerRepository = tripPassengerRepository;
		this.groupMemberRepository = groupMemberRepository;
	}

	public void updateRatios(GroupMember member) {
		Double tripsAsPassenger = tripPassengerRepository.findAssistanceAsStatus(member.getId(),
				PassengerStatus.PASSENGER.toString());
		Double tripsAsDriver = tripPassengerRepository.findAssistanceAsStatus(member.getId(),
				PassengerStatus.DRIVES.toString());
		Double newRatio;
		if (tripsAsDriver + tripsAsPassenger == 0) {
			// The member has not made any trips, set the ratio to zero
			newRatio = 0.0;
		} else {
			newRatio = (tripsAsDriver / (tripsAsDriver + tripsAsPassenger)) * 100;
		}
		member.setDriverRatio(newRatio);
		this.groupMemberRepository.save(member);
	}

	public List<TripPassenger> driversTomorrow() {
		return tripPassengerRepository.findDriversForDate(ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).plusDays(1),
				ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).plusDays(2));
	}

	/**
	 * Save a tripDay.
	 *
	 * @param tripDay the entity to save.
	 * @return the persisted entity.
	 */
	public TripDay save(TripDay tripDay) {
		log.debug("Request to save TripDay : {}", tripDay);
		auditHelperService.updateAuditFields(tripDay, id -> tripDayRepository.findByIdAndActiveTrue(id));
		return tripDayRepository.save(tripDay);
	}

	/**
	 * Get all the tripDays.
	 *
	 * @return the list of entities.
	 */
	@Transactional(readOnly = true)
	public List<TripDay> findAll() {
		log.debug("Request to get all TripDays");
		return tripDayRepository.findAllByActiveTrue();
	}

	/**
	 * Get one tripDay by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Transactional(readOnly = true)
	public Optional<TripDay> findOne(Long id) {
		log.debug("Request to get TripDay : {}", id);
		return tripDayRepository.findByIdAndActiveTrue(id);
	}

	/**
	 * Delete the tripDay by id.
	 *
	 * @param id the id of the entity.
	 */
	public void delete(Long id) {
		log.debug("Request to delete TripDay : {}", id);
		Optional<TripDay> tripDay = tripDayRepository.findByIdAndActiveTrue(id);
		if (tripDay.isPresent()) {
			// Mark all the passengers as inactive
			findTripPassengers(id).stream().forEach(passenger -> {
				tripPassengerRepository.delete(passenger);
				updateRatios(passenger.getGroupMember());
			});
			auditHelperService.markAsInactive(tripDay.get());
			tripDayRepository.save(tripDay.get());
		}
	}

	public List<TripPassenger> findTripPassengers(Long tripDayId) {
		log.debug("Request to get TripDay passengers for : {}", tripDayId);
		Optional<TripDay> day = tripDayRepository.findByIdAndActiveTrue(tripDayId);
		if (!day.isPresent()) {
			return new ArrayList<>();
		}
		return tripPassengerRepository.findByTripDayAndActiveTrue(tripDayRepository.findById(tripDayId).get());
	}

	public TripPassenger save(TripPassenger tripPassenger) {
		auditHelperService.updateAuditFields(tripPassenger, id -> tripPassengerRepository.findByIdAndActiveTrue(id));
		return tripPassengerRepository.save(tripPassenger);
	}
}