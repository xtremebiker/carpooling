package org.xtremebiker.carpooling.service;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Service;
import org.xtremebiker.carpooling.domain.Auditable;

/**
 * Audit Helper utilities
 */
@Service
public class AuditHelperService {

    public String loggedUsername() {
        if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null) {
            return ((DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPreferredUsername();
        }
        return "";
    }

    public <T extends Auditable> void updateAuditFields(T auditable, Function<Long, Optional<T>> load) {
        if (auditable.getId() == null) {
            auditable.setCreated(ZonedDateTime.now());
            auditable.setCreatedBy(loggedUsername());
        } else {
            Auditable stored = load.apply(auditable.getId()).get();
            auditable.setCreated(stored.getCreated());
            auditable.setCreatedBy(stored.getCreatedBy());
            auditable.setUpdated(ZonedDateTime.now());
            auditable.setUpdatedBy(loggedUsername());
        }
        auditable.setActive(true);
    }

    public <T extends Auditable> void markAsInactive(T auditable) {
        auditable.setUpdated(ZonedDateTime.now());
        auditable.setUpdatedBy(loggedUsername());
        auditable.setActive(false);
    }

}