package org.xtremebiker.carpooling.service;

import org.xtremebiker.carpooling.domain.TripGroup;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link TripGroup}.
 */
public interface TripGroupService {

	/**
	 * Save a tripGroup.
	 *
	 * @param tripGroup the entity to save.
	 * @return the persisted entity.
	 */
	TripGroup save(TripGroup tripGroup);

	/**
	 * Get all the tripGroups.
	 *
	 * @return the list of entities.
	 */
	List<TripGroup> findAll();

	/**
	 * Get the "id" tripGroup.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<TripGroup> findOne(Long id);

	/**
	 * Delete the "id" tripGroup.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

	/**
	 * Find all the trip groups for a given user id
	 * 
	 * @param idAuth
	 * @return
	 */
	List<TripGroup> findAllByUser(String idAuth);
}
