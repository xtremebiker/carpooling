package org.xtremebiker.carpooling.repository;

import org.xtremebiker.carpooling.domain.TripGroup;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TripGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripGroupRepository extends JpaRepository<TripGroup, Long> {

    public List<TripGroup> findAllByActiveTrue();

    public Optional<TripGroup> findByIdAndActiveTrue(Long id);

}
