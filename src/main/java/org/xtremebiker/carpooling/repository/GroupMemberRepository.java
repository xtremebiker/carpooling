package org.xtremebiker.carpooling.repository;

import org.xtremebiker.carpooling.domain.GroupMember;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the GroupMember entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMember, Long> {

	List<GroupMember> findAllByActiveTrue();

	List<GroupMember> findByAuthIdAndActiveTrue(String authId);

	Optional<GroupMember> findByIdAndActiveTrue(Long id);
	
	List<GroupMember> findByTripGroupIdAndActiveTrue(Long tripGroupId);

}
