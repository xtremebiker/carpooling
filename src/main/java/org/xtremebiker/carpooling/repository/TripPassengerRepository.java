package org.xtremebiker.carpooling.repository;

import org.xtremebiker.carpooling.domain.TripDay;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.domain.enumeration.PassengerStatus;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the TripPassenger entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripPassengerRepository extends JpaRepository<TripPassenger, Long> {

	public List<TripPassenger> findByTripDayAndActiveTrue(TripDay tripDay);

	List<TripPassenger> findAllByActiveTrue();

	Optional<TripPassenger> findByIdAndActiveTrue(Long id);

	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM TRIP_PASSENGER tp JOIN GROUP_MEMBER gm ON tp.GROUP_MEMBER_ID = gm.ID WHERE gm.ID = :idGroupMember AND tp.PASSENGER_STATUS = :passengerStatus and tp.ACTIVE = 1")
	Double findAssistanceAsStatus(@Param("idGroupMember") Long idGroupMember,
			@Param("passengerStatus") String passengerStatus);

	@Query("SELECT passenger FROM TripPassenger passenger JOIN passenger.tripDay AS tripDay where tripDay.tripDate BETWEEN :date1 AND :date2 AND passenger.passengerStatus = 'DRIVES' AND passenger.active = true AND tripDay.active = true")
	List<TripPassenger> findDriversForDate(@Param("date1") ZonedDateTime date1, @Param("date2") ZonedDateTime date2);

}
