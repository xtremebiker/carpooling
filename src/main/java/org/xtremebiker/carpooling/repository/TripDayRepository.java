package org.xtremebiker.carpooling.repository;

import org.xtremebiker.carpooling.domain.TripDay;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TripDay entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripDayRepository extends JpaRepository<TripDay, Long> {

    List<TripDay> findByTripGroupIdAndTripDateBetweenAndActiveTrueOrderByTripDate(Long groupId, ZonedDateTime date1, ZonedDateTime date2);
    
    List<TripDay> findByTripGroupIdAndTripDateBetweenAndActiveTrueOrderByTripDateDesc(Long groupId, ZonedDateTime date1, ZonedDateTime date2);

    List<TripDay> findAllByActiveTrue();

    Optional<TripDay> findByIdAndActiveTrue(Long id);

}
