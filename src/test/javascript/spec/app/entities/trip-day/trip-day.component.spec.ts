/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CarpoolingTestModule } from '../../../test.module';
import { TripDayComponent } from 'app/entities/trip-day/trip-day.component';
import { TripDayService } from 'app/entities/trip-day/trip-day.service';
import { TripDay } from 'app/shared/model/trip-day.model';

describe('Component Tests', () => {
  describe('TripDay Management Component', () => {
    let comp: TripDayComponent;
    let fixture: ComponentFixture<TripDayComponent>;
    let service: TripDayService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripDayComponent],
        providers: []
      })
        .overrideTemplate(TripDayComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TripDayComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripDayService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TripDay(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.tripDays[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
