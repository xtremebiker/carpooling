/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CarpoolingTestModule } from '../../../test.module';
import { TripDayDetailComponent } from 'app/entities/trip-day/trip-day-detail.component';
import { TripDay } from 'app/shared/model/trip-day.model';

describe('Component Tests', () => {
  describe('TripDay Management Detail Component', () => {
    let comp: TripDayDetailComponent;
    let fixture: ComponentFixture<TripDayDetailComponent>;
    const route = ({ data: of({ tripDay: new TripDay(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripDayDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TripDayDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TripDayDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tripDay).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
