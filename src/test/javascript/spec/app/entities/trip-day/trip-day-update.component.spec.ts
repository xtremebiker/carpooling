/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CarpoolingTestModule } from '../../../test.module';
import { TripDayUpdateComponent } from 'app/entities/trip-day/trip-day-update.component';
import { TripDayService } from 'app/entities/trip-day/trip-day.service';
import { TripDay } from 'app/shared/model/trip-day.model';

describe('Component Tests', () => {
  describe('TripDay Management Update Component', () => {
    let comp: TripDayUpdateComponent;
    let fixture: ComponentFixture<TripDayUpdateComponent>;
    let service: TripDayService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripDayUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TripDayUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TripDayUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripDayService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TripDay(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TripDay();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
