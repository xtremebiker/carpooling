/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CarpoolingTestModule } from '../../../test.module';
import { TripGroupComponent } from 'app/entities/trip-group/trip-group.component';
import { TripGroupService } from 'app/entities/trip-group/trip-group.service';
import { TripGroup } from 'app/shared/model/trip-group.model';

describe('Component Tests', () => {
  describe('TripGroup Management Component', () => {
    let comp: TripGroupComponent;
    let fixture: ComponentFixture<TripGroupComponent>;
    let service: TripGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripGroupComponent],
        providers: []
      })
        .overrideTemplate(TripGroupComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TripGroupComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripGroupService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TripGroup(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.tripGroups[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
