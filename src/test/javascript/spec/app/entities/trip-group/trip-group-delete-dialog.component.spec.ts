/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CarpoolingTestModule } from '../../../test.module';
import { TripGroupDeleteDialogComponent } from 'app/entities/trip-group/trip-group-delete-dialog.component';
import { TripGroupService } from 'app/entities/trip-group/trip-group.service';

describe('Component Tests', () => {
  describe('TripGroup Management Delete Component', () => {
    let comp: TripGroupDeleteDialogComponent;
    let fixture: ComponentFixture<TripGroupDeleteDialogComponent>;
    let service: TripGroupService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripGroupDeleteDialogComponent]
      })
        .overrideTemplate(TripGroupDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TripGroupDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripGroupService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
