/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CarpoolingTestModule } from '../../../test.module';
import { TripGroupDetailComponent } from 'app/entities/trip-group/trip-group-detail.component';
import { TripGroup } from 'app/shared/model/trip-group.model';

describe('Component Tests', () => {
  describe('TripGroup Management Detail Component', () => {
    let comp: TripGroupDetailComponent;
    let fixture: ComponentFixture<TripGroupDetailComponent>;
    const route = ({ data: of({ tripGroup: new TripGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TripGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TripGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tripGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
