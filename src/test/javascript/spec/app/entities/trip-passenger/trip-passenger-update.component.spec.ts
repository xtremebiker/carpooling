/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { CarpoolingTestModule } from '../../../test.module';
import { TripPassengerUpdateComponent } from 'app/entities/trip-passenger/trip-passenger-update.component';
import { TripPassengerService } from 'app/entities/trip-passenger/trip-passenger.service';
import { TripPassenger } from 'app/shared/model/trip-passenger.model';

describe('Component Tests', () => {
  describe('TripPassenger Management Update Component', () => {
    let comp: TripPassengerUpdateComponent;
    let fixture: ComponentFixture<TripPassengerUpdateComponent>;
    let service: TripPassengerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripPassengerUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TripPassengerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TripPassengerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripPassengerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TripPassenger(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TripPassenger();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
