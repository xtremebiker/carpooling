/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { TripPassengerService } from 'app/entities/trip-passenger/trip-passenger.service';
import { ITripPassenger, TripPassenger, PassengerStatus } from 'app/shared/model/trip-passenger.model';

describe('Service Tests', () => {
  describe('TripPassenger Service', () => {
    let injector: TestBed;
    let service: TripPassengerService;
    let httpMock: HttpTestingController;
    let elemDefault: ITripPassenger;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(TripPassengerService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new TripPassenger(0, PassengerStatus.DRIVES, 0, false, currentDate, 'AAAAAAA', currentDate, 'AAAAAAA', false);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a TripPassenger', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .create(new TripPassenger(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a TripPassenger', async () => {
        const returnedFromService = Object.assign(
          {
            passengerStatus: 'BBBBBB',
            ratio: 1,
            canDrive: true,
            created: currentDate.format(DATE_TIME_FORMAT),
            createdBy: 'BBBBBB',
            updated: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            active: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of TripPassenger', async () => {
        const returnedFromService = Object.assign(
          {
            passengerStatus: 'BBBBBB',
            ratio: 1,
            canDrive: true,
            created: currentDate.format(DATE_TIME_FORMAT),
            createdBy: 'BBBBBB',
            updated: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            active: true
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TripPassenger', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
