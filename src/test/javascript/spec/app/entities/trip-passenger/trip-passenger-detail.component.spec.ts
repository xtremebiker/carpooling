/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CarpoolingTestModule } from '../../../test.module';
import { TripPassengerDetailComponent } from 'app/entities/trip-passenger/trip-passenger-detail.component';
import { TripPassenger } from 'app/shared/model/trip-passenger.model';

describe('Component Tests', () => {
  describe('TripPassenger Management Detail Component', () => {
    let comp: TripPassengerDetailComponent;
    let fixture: ComponentFixture<TripPassengerDetailComponent>;
    const route = ({ data: of({ tripPassenger: new TripPassenger(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripPassengerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TripPassengerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TripPassengerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tripPassenger).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
