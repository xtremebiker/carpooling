/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CarpoolingTestModule } from '../../../test.module';
import { TripPassengerComponent } from 'app/entities/trip-passenger/trip-passenger.component';
import { TripPassengerService } from 'app/entities/trip-passenger/trip-passenger.service';
import { TripPassenger } from 'app/shared/model/trip-passenger.model';

describe('Component Tests', () => {
  describe('TripPassenger Management Component', () => {
    let comp: TripPassengerComponent;
    let fixture: ComponentFixture<TripPassengerComponent>;
    let service: TripPassengerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [TripPassengerComponent],
        providers: []
      })
        .overrideTemplate(TripPassengerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TripPassengerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TripPassengerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TripPassenger(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.tripPassengers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
