/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CarpoolingTestModule } from '../../../test.module';
import { GroupMemberComponent } from 'app/entities/group-member/group-member.component';
import { GroupMemberService } from 'app/entities/group-member/group-member.service';
import { GroupMember } from 'app/shared/model/group-member.model';

describe('Component Tests', () => {
  describe('GroupMember Management Component', () => {
    let comp: GroupMemberComponent;
    let fixture: ComponentFixture<GroupMemberComponent>;
    let service: GroupMemberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CarpoolingTestModule],
        declarations: [GroupMemberComponent],
        providers: []
      })
        .overrideTemplate(GroupMemberComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(GroupMemberComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(GroupMemberService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new GroupMember(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.groupMembers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
