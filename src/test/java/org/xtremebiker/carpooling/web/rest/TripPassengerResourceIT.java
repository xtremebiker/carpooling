package org.xtremebiker.carpooling.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.xtremebiker.carpooling.web.rest.TestUtil.createFormattingConversionService;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.xtremebiker.carpooling.CarpoolingApp;
import org.xtremebiker.carpooling.config.TestSecurityConfiguration;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.domain.enumeration.PassengerStatus;
import org.xtremebiker.carpooling.repository.GroupMemberRepository;
import org.xtremebiker.carpooling.repository.TripPassengerRepository;
import org.xtremebiker.carpooling.service.AuditHelperService;
import org.xtremebiker.carpooling.service.impl.TripDayServiceImpl;
import org.xtremebiker.carpooling.web.rest.errors.ExceptionTranslator;

/**
 * Integration tests for the {@Link TripPassengerResource} REST controller.
 */
@SpringBootTest(classes = { CarpoolingApp.class, TestSecurityConfiguration.class })
public class TripPassengerResourceIT {

	private static final PassengerStatus DEFAULT_PASSENGER_STATUS = PassengerStatus.DRIVES;
	private static final PassengerStatus UPDATED_PASSENGER_STATUS = PassengerStatus.PASSENGER;

	private static final Double DEFAULT_RATIO = 1D;
	private static final Double UPDATED_RATIO = 2D;

	private static final Boolean DEFAULT_CAN_DRIVE = false;
	private static final Boolean UPDATED_CAN_DRIVE = true;

	private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
			ZoneOffset.UTC);
	private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

	private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
	private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

	private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
			ZoneOffset.UTC);
	private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

	private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
	private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

	private static final Boolean DEFAULT_ACTIVE = true;
	private static final Boolean UPDATED_ACTIVE = true;

	@Autowired
	private TripPassengerRepository tripPassengerRepository;

	@Autowired
	private GroupMemberRepository groupMemberRepository;

	@Autowired
	private TripDayServiceImpl tripDayServiceImpl;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	@Autowired
	private Validator validator;

	private MockMvc restTripPassengerMockMvc;

	private TripPassenger tripPassenger;

	@Autowired
	private AuditHelperService auditHelperService;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final TripPassengerResource tripPassengerResource = new TripPassengerResource(auditHelperService,
				tripPassengerRepository, groupMemberRepository, tripDayServiceImpl);
		this.restTripPassengerMockMvc = MockMvcBuilders.standaloneSetup(tripPassengerResource)
				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
				.setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
				.setValidator(validator).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static TripPassenger createEntity(EntityManager em) {
		TripPassenger tripPassenger = new TripPassenger().passengerStatus(DEFAULT_PASSENGER_STATUS).ratio(DEFAULT_RATIO)
				.canDrive(DEFAULT_CAN_DRIVE).created(DEFAULT_CREATED).createdBy(DEFAULT_CREATED_BY)
				.updated(DEFAULT_UPDATED).updatedBy(DEFAULT_UPDATED_BY).active(DEFAULT_ACTIVE);
		return tripPassenger;
	}

	/**
	 * Create an updated entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it, if
	 * they test an entity which requires the current entity.
	 */
	public static TripPassenger createUpdatedEntity(EntityManager em) {
		TripPassenger tripPassenger = new TripPassenger().passengerStatus(UPDATED_PASSENGER_STATUS).ratio(UPDATED_RATIO)
				.canDrive(UPDATED_CAN_DRIVE).created(UPDATED_CREATED).createdBy(UPDATED_CREATED_BY)
				.updated(UPDATED_UPDATED).updatedBy(UPDATED_UPDATED_BY).active(UPDATED_ACTIVE);
		return tripPassenger;
	}

	@BeforeEach
	public void initTest() {
		tripPassenger = createEntity(em);
	}

	@Test
	@Transactional
	@Disabled
	public void createTripPassenger() throws Exception {
		int databaseSizeBeforeCreate = tripPassengerRepository.findAllByActiveTrue().size();

		// Create the TripPassenger
		restTripPassengerMockMvc.perform(post("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(tripPassenger))).andExpect(status().isCreated());

		// Validate the TripPassenger in the database
		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeCreate + 1);
		TripPassenger testTripPassenger = tripPassengerList.get(tripPassengerList.size() - 1);
		assertThat(testTripPassenger.getPassengerStatus()).isEqualTo(DEFAULT_PASSENGER_STATUS);
		assertThat(testTripPassenger.getRatio()).isEqualTo(DEFAULT_RATIO);
		assertThat(testTripPassenger.isCanDrive()).isEqualTo(DEFAULT_CAN_DRIVE);
	}

	@Test
	@Transactional
	@Disabled
	public void createTripPassengerWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = tripPassengerRepository.findAllByActiveTrue().size();

		// Create the TripPassenger with an existing ID
		tripPassenger.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restTripPassengerMockMvc.perform(post("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(tripPassenger))).andExpect(status().isBadRequest());

		// Validate the TripPassenger in the database
		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkPassengerStatusIsRequired() throws Exception {
		int databaseSizeBeforeTest = tripPassengerRepository.findAllByActiveTrue().size();
		// set the field null
		tripPassenger.setPassengerStatus(null);

		// Create the TripPassenger, which fails.

		restTripPassengerMockMvc.perform(post("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(tripPassenger))).andExpect(status().isBadRequest());

		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void checkRatioIsRequired() throws Exception {
		int databaseSizeBeforeTest = tripPassengerRepository.findAllByActiveTrue().size();
		// set the field null
		tripPassenger.setRatio(null);

		// Create the TripPassenger, which fails.

		restTripPassengerMockMvc.perform(post("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(tripPassenger))).andExpect(status().isBadRequest());

		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllTripPassengers() throws Exception {
		// Initialize the database
		tripPassengerRepository.saveAndFlush(tripPassenger);

		// Get all the tripPassengerList
		restTripPassengerMockMvc.perform(get("/api/trip-passengers?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(tripPassenger.getId().intValue())))
				.andExpect(jsonPath("$.[*].passengerStatus").value(hasItem(DEFAULT_PASSENGER_STATUS.toString())))
				.andExpect(jsonPath("$.[*].ratio").value(hasItem(DEFAULT_RATIO.doubleValue())))
				.andExpect(jsonPath("$.[*].canDrive").value(hasItem(DEFAULT_CAN_DRIVE.booleanValue())));
	}

	@Test
	@Transactional
	public void getTripPassenger() throws Exception {
		// Initialize the database
		tripPassengerRepository.saveAndFlush(tripPassenger);

		// Get the tripPassenger
		restTripPassengerMockMvc.perform(get("/api/trip-passengers/{id}", tripPassenger.getId()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(tripPassenger.getId().intValue()))
				.andExpect(jsonPath("$.passengerStatus").value(DEFAULT_PASSENGER_STATUS.toString()))
				.andExpect(jsonPath("$.ratio").value(DEFAULT_RATIO.doubleValue()))
				.andExpect(jsonPath("$.canDrive").value(DEFAULT_CAN_DRIVE.booleanValue()));
	}

	@Test
	@Transactional
	public void getNonExistingTripPassenger() throws Exception {
		// Get the tripPassenger
		restTripPassengerMockMvc.perform(get("/api/trip-passengers/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@Disabled
	public void updateTripPassenger() throws Exception {
		// Initialize the database
		tripPassengerRepository.saveAndFlush(tripPassenger);

		int databaseSizeBeforeUpdate = tripPassengerRepository.findAll().size();

		// Update the tripPassenger
		TripPassenger updatedTripPassenger = tripPassengerRepository.findById(tripPassenger.getId()).get();
		// Disconnect from session so that the updates on updatedTripPassenger are not
		// directly saved in db
		em.detach(updatedTripPassenger);
		updatedTripPassenger.passengerStatus(UPDATED_PASSENGER_STATUS).ratio(UPDATED_RATIO).canDrive(UPDATED_CAN_DRIVE);

		restTripPassengerMockMvc.perform(put("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedTripPassenger))).andExpect(status().isOk());

		// Validate the TripPassenger in the database
		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAll();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeUpdate);
		TripPassenger testTripPassenger = tripPassengerList.get(tripPassengerList.size() - 1);
		assertThat(testTripPassenger.getPassengerStatus()).isEqualTo(UPDATED_PASSENGER_STATUS);
		assertThat(testTripPassenger.getRatio()).isEqualTo(UPDATED_RATIO);
		assertThat(testTripPassenger.isCanDrive()).isEqualTo(UPDATED_CAN_DRIVE);
	}

	@Test
	@Transactional
	public void updateNonExistingTripPassenger() throws Exception {
		int databaseSizeBeforeUpdate = tripPassengerRepository.findAllByActiveTrue().size();

		// Create the TripPassenger

		// If the entity doesn't have an ID, it will throw BadRequestAlertException
		restTripPassengerMockMvc.perform(put("/api/trip-passengers").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(tripPassenger))).andExpect(status().isBadRequest());

		// Validate the TripPassenger in the database
		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	@Disabled
	public void deleteTripPassenger() throws Exception {
		// Initialize the database
		tripPassengerRepository.saveAndFlush(tripPassenger);

		int databaseSizeBeforeDelete = tripPassengerRepository.findAllByActiveTrue().size();

		// Delete the tripPassenger
		restTripPassengerMockMvc.perform(
				delete("/api/trip-passengers/{id}", tripPassenger.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isNoContent());

		// Validate the database is empty
		List<TripPassenger> tripPassengerList = tripPassengerRepository.findAllByActiveTrue();
		assertThat(tripPassengerList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(TripPassenger.class);
		TripPassenger tripPassenger1 = new TripPassenger();
		tripPassenger1.setId(1L);
		TripPassenger tripPassenger2 = new TripPassenger();
		tripPassenger2.setId(tripPassenger1.getId());
		assertThat(tripPassenger1).isEqualTo(tripPassenger2);
		tripPassenger2.setId(2L);
		assertThat(tripPassenger1).isNotEqualTo(tripPassenger2);
		tripPassenger1.setId(null);
		assertThat(tripPassenger1).isNotEqualTo(tripPassenger2);
	}
}
