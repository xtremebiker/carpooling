package org.xtremebiker.carpooling.web.rest;

import org.xtremebiker.carpooling.CarpoolingApp;
import org.xtremebiker.carpooling.config.TestSecurityConfiguration;
import org.xtremebiker.carpooling.domain.GroupMember;
import org.xtremebiker.carpooling.repository.GroupMemberRepository;
import org.xtremebiker.carpooling.service.AuditHelperService;
import org.xtremebiker.carpooling.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.xtremebiker.carpooling.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.xtremebiker.carpooling.domain.enumeration.Role;
/**
 * Integration tests for the {@Link GroupMemberResource} REST controller.
 */
@SpringBootTest(classes = {CarpoolingApp.class, TestSecurityConfiguration.class})
public class GroupMemberResourceIT {

    private static final String DEFAULT_AUTH_ID = "AAAAAAAAAA";
    private static final String UPDATED_AUTH_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CAN_DRIVE = false;
    private static final Boolean UPDATED_CAN_DRIVE = true;

    private static final Role DEFAULT_PERMISSION = Role.USER;
    private static final Role UPDATED_PERMISSION = Role.ADMIN;

    private static final Double DEFAULT_DRIVER_RATIO = 1D;
    private static final Double UPDATED_DRIVER_RATIO = 2D;

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = true;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private AuditHelperService auditHelperService;

    private MockMvc restGroupMemberMockMvc;

    private GroupMember groupMember;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GroupMemberResource groupMemberResource = new GroupMemberResource(auditHelperService, groupMemberRepository);
        this.restGroupMemberMockMvc = MockMvcBuilders.standaloneSetup(groupMemberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GroupMember createEntity(EntityManager em) {
        GroupMember groupMember = new GroupMember()
            .authId(DEFAULT_AUTH_ID)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .canDrive(DEFAULT_CAN_DRIVE)
            .permission(DEFAULT_PERMISSION)
            .driverRatio(DEFAULT_DRIVER_RATIO)
            .created(DEFAULT_CREATED)
            .createdBy(DEFAULT_CREATED_BY)
            .updated(DEFAULT_UPDATED)
            .updatedBy(DEFAULT_UPDATED_BY)
            .active(DEFAULT_ACTIVE);
        return groupMember;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GroupMember createUpdatedEntity(EntityManager em) {
        GroupMember groupMember = new GroupMember()
            .authId(UPDATED_AUTH_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .canDrive(UPDATED_CAN_DRIVE)
            .permission(UPDATED_PERMISSION)
            .driverRatio(UPDATED_DRIVER_RATIO)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);
        return groupMember;
    }

    @BeforeEach
    public void initTest() {
        groupMember = createEntity(em);
    }

    @Test
    @Transactional
    public void createGroupMember() throws Exception {
        int databaseSizeBeforeCreate = groupMemberRepository.findAllByActiveTrue().size();

        // Create the GroupMember
        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isCreated());

        // Validate the GroupMember in the database
        List<GroupMember> groupMemberList = groupMemberRepository.findAllByActiveTrue();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeCreate + 1);
        GroupMember testGroupMember = groupMemberList.get(groupMemberList.size() - 1);
        assertThat(testGroupMember.getAuthId()).isEqualTo(DEFAULT_AUTH_ID);
        assertThat(testGroupMember.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testGroupMember.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testGroupMember.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testGroupMember.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testGroupMember.isCanDrive()).isEqualTo(DEFAULT_CAN_DRIVE);
        assertThat(testGroupMember.getPermission()).isEqualTo(DEFAULT_PERMISSION);
        assertThat(testGroupMember.getDriverRatio()).isEqualTo(DEFAULT_DRIVER_RATIO);
    }

    @Test
    @Transactional
    public void createGroupMemberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = groupMemberRepository.findAll().size();

        // Create the GroupMember with an existing ID
        groupMember.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        // Validate the GroupMember in the database
        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAuthIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setAuthId(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setFirstName(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setLastName(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setEmail(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPermissionIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setPermission(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDriverRatioIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMemberRepository.findAll().size();
        // set the field null
        groupMember.setDriverRatio(null);

        // Create the GroupMember, which fails.

        restGroupMemberMockMvc.perform(post("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGroupMembers() throws Exception {
        // Initialize the database
        groupMemberRepository.saveAndFlush(groupMember);

        // Get all the groupMemberList
        restGroupMemberMockMvc.perform(get("/api/group-members?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(groupMember.getId().intValue())))
            .andExpect(jsonPath("$.[*].authId").value(hasItem(DEFAULT_AUTH_ID.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].canDrive").value(hasItem(DEFAULT_CAN_DRIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].permission").value(hasItem(DEFAULT_PERMISSION.toString())))
            .andExpect(jsonPath("$.[*].driverRatio").value(hasItem(DEFAULT_DRIVER_RATIO.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getGroupMember() throws Exception {
        // Initialize the database
        groupMemberRepository.saveAndFlush(groupMember);

        // Get the groupMember
        restGroupMemberMockMvc.perform(get("/api/group-members/{id}", groupMember.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(groupMember.getId().intValue()))
            .andExpect(jsonPath("$.authId").value(DEFAULT_AUTH_ID.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.canDrive").value(DEFAULT_CAN_DRIVE.booleanValue()))
            .andExpect(jsonPath("$.permission").value(DEFAULT_PERMISSION.toString()))
            .andExpect(jsonPath("$.driverRatio").value(DEFAULT_DRIVER_RATIO.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGroupMember() throws Exception {
        // Get the groupMember
        restGroupMemberMockMvc.perform(get("/api/group-members/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGroupMember() throws Exception {
        // Initialize the database
        groupMemberRepository.saveAndFlush(groupMember);

        int databaseSizeBeforeUpdate = groupMemberRepository.findAllByActiveTrue().size();

        // Update the groupMember
        GroupMember updatedGroupMember = groupMemberRepository.findByIdAndActiveTrue(groupMember.getId()).get();
        // Disconnect from session so that the updates on updatedGroupMember are not directly saved in db
        em.detach(updatedGroupMember);
        updatedGroupMember
            .authId(UPDATED_AUTH_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .canDrive(UPDATED_CAN_DRIVE)
            .permission(UPDATED_PERMISSION)
            .driverRatio(UPDATED_DRIVER_RATIO);

        restGroupMemberMockMvc.perform(put("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGroupMember)))
            .andExpect(status().isOk());

        // Validate the GroupMember in the database
        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeUpdate);
        GroupMember testGroupMember = groupMemberList.get(groupMemberList.size() - 1);
        assertThat(testGroupMember.getAuthId()).isEqualTo(UPDATED_AUTH_ID);
        assertThat(testGroupMember.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testGroupMember.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testGroupMember.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testGroupMember.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testGroupMember.isCanDrive()).isEqualTo(UPDATED_CAN_DRIVE);
        assertThat(testGroupMember.getPermission()).isEqualTo(UPDATED_PERMISSION);
        assertThat(testGroupMember.getDriverRatio()).isEqualTo(UPDATED_DRIVER_RATIO);
    }

    @Test
    @Transactional
    public void updateNonExistingGroupMember() throws Exception {
        int databaseSizeBeforeUpdate = groupMemberRepository.findAllByActiveTrue().size();

        // Create the GroupMember

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGroupMemberMockMvc.perform(put("/api/group-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(groupMember)))
            .andExpect(status().isBadRequest());

        // Validate the GroupMember in the database
        List<GroupMember> groupMemberList = groupMemberRepository.findAllByActiveTrue();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGroupMember() throws Exception {
        // Initialize the database
        groupMemberRepository.saveAndFlush(groupMember);

        int databaseSizeBeforeDelete = groupMemberRepository.findAllByActiveTrue().size();

        // Delete the groupMember
        restGroupMemberMockMvc.perform(delete("/api/group-members/{id}", groupMember.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<GroupMember> groupMemberList = groupMemberRepository.findAllByActiveTrue();
        assertThat(groupMemberList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GroupMember.class);
        GroupMember groupMember1 = new GroupMember();
        groupMember1.setId(1L);
        GroupMember groupMember2 = new GroupMember();
        groupMember2.setId(groupMember1.getId());
        assertThat(groupMember1).isEqualTo(groupMember2);
        groupMember2.setId(2L);
        assertThat(groupMember1).isNotEqualTo(groupMember2);
        groupMember1.setId(null);
        assertThat(groupMember1).isNotEqualTo(groupMember2);
    }
}
