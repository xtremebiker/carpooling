package org.xtremebiker.carpooling.web.rest;

import org.xtremebiker.carpooling.CarpoolingApp;
import org.xtremebiker.carpooling.config.TestSecurityConfiguration;
import org.xtremebiker.carpooling.domain.TripDay;
import org.xtremebiker.carpooling.domain.TripPassenger;
import org.xtremebiker.carpooling.domain.enumeration.PassengerStatus;
import org.xtremebiker.carpooling.repository.TripDayRepository;
import org.xtremebiker.carpooling.repository.TripPassengerRepository;
import org.xtremebiker.carpooling.service.AuditHelperService;
import org.xtremebiker.carpooling.service.impl.TripDayServiceImpl;
import org.xtremebiker.carpooling.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.xtremebiker.carpooling.web.rest.TestUtil.sameInstant;
import static org.xtremebiker.carpooling.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TripDayResource} REST controller.
 */
@SpringBootTest(classes = {CarpoolingApp.class, TestSecurityConfiguration.class})
public class TripDayResourceIT {

    private static final ZonedDateTime DEFAULT_TRIP_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TRIP_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = true;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private TripDayRepository tripDayRepository;

    @Autowired
    private TripPassengerRepository tripPassengerRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTripDayMockMvc;

    private TripDay tripDay;

    @Autowired
    private TripDayServiceImpl tripDayService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripDayResource tripDayResource = new TripDayResource(tripDayService);
        this.restTripDayMockMvc = MockMvcBuilders.standaloneSetup(tripDayResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripDay createEntity(EntityManager em) {
        TripDay tripDay = new TripDay()
            .tripDate(DEFAULT_TRIP_DATE)
            .created(DEFAULT_CREATED)
            .createdBy(DEFAULT_CREATED_BY)
            .updated(DEFAULT_UPDATED)
            .updatedBy(DEFAULT_UPDATED_BY)
            .active(DEFAULT_ACTIVE);
        return tripDay;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripDay createUpdatedEntity(EntityManager em) {
        TripDay tripDay = new TripDay()
            .tripDate(UPDATED_TRIP_DATE)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);
        return tripDay;
    }

    @BeforeEach
    public void initTest() {
        tripDay = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripDay() throws Exception {
        int databaseSizeBeforeCreate = tripDayRepository.findAll().size();

        // Create the TripDay
        restTripDayMockMvc.perform(post("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDay)))
            .andExpect(status().isCreated());

        // Validate the TripDay in the database
        List<TripDay> tripDayList = tripDayRepository.findAll();
        assertThat(tripDayList).hasSize(databaseSizeBeforeCreate + 1);
        TripDay testTripDay = tripDayList.get(tripDayList.size() - 1);
        assertThat(testTripDay.getTripDate()).isEqualTo(DEFAULT_TRIP_DATE);
    }

    @Test
    @Transactional
    public void createTripDayWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripDayRepository.findAll().size();

        // Create the TripDay with an existing ID
        tripDay.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripDayMockMvc.perform(post("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDay)))
            .andExpect(status().isBadRequest());

        // Validate the TripDay in the database
        List<TripDay> tripDayList = tripDayRepository.findAll();
        assertThat(tripDayList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTripDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = tripDayRepository.findAll().size();
        // set the field null
        tripDay.setTripDate(null);

        // Create the TripDay, which fails.

        restTripDayMockMvc.perform(post("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDay)))
            .andExpect(status().isBadRequest());

        List<TripDay> tripDayList = tripDayRepository.findAll();
        assertThat(tripDayList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTripDays() throws Exception {
        // Initialize the database
        tripDayRepository.saveAndFlush(tripDay);

        // Get all the tripDayList
        restTripDayMockMvc.perform(get("/api/trip-days?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripDay.getId().intValue())))
            .andExpect(jsonPath("$.[*].tripDate").value(hasItem(sameInstant(DEFAULT_TRIP_DATE))));
    }
    
    @Test
    @Transactional
    public void getTripDay() throws Exception {
        // Initialize the database
        tripDayRepository.saveAndFlush(tripDay);

        // Get the tripDay
        restTripDayMockMvc.perform(get("/api/trip-days/{id}", tripDay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripDay.getId().intValue()))
            .andExpect(jsonPath("$.tripDate").value(sameInstant(DEFAULT_TRIP_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingTripDay() throws Exception {
        // Get the tripDay
        restTripDayMockMvc.perform(get("/api/trip-days/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripDay() throws Exception {
        // Initialize the database
        tripDayRepository.saveAndFlush(tripDay);

        int databaseSizeBeforeUpdate = tripDayRepository.findAllByActiveTrue().size();

        // Update the tripDay
        TripDay updatedTripDay = tripDayRepository.findByIdAndActiveTrue(tripDay.getId()).get();
        // Disconnect from session so that the updates on updatedTripDay are not directly saved in db
        em.detach(updatedTripDay);
        updatedTripDay
            .tripDate(UPDATED_TRIP_DATE)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);

        restTripDayMockMvc.perform(put("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripDay)))
            .andExpect(status().isOk());

        // Validate the TripDay in the database
        List<TripDay> tripDayList = tripDayRepository.findAllByActiveTrue();
        assertThat(tripDayList).hasSize(databaseSizeBeforeUpdate);
        TripDay testTripDay = tripDayList.get(tripDayList.size() - 1);
        assertThat(testTripDay.getTripDate()).isEqualTo(UPDATED_TRIP_DATE);
    }

    @Test
    @Transactional
    public void updateTripPassenger() throws Exception {
        // Initialize the database
        tripDayRepository.saveAndFlush(tripDay);

        int databaseSizeBeforeUpdate = tripPassengerRepository.findAllByActiveTrue().size();

        // Save the tripDay
        TripDay updatedTripDay = tripDayRepository.save(tripDay);
        // Disconnect from session so that the updates on updatedTripDay are not directly saved in db
        em.detach(updatedTripDay);
        updatedTripDay
            .tripDate(UPDATED_TRIP_DATE)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);

        restTripDayMockMvc.perform(put("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripDay)))
            .andExpect(status().isOk());

        restTripDayMockMvc.perform(put("/api/trip-days/" + updatedTripDay.getId() + "/passengers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(new TripPassenger()
            .tripDay(updatedTripDay).passengerStatus(PassengerStatus.NOT_GOING).ratio(1.0))))
            .andExpect(status().isOk());

        assertThat(tripPassengerRepository.findAllByActiveTrue()).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTripDay() throws Exception {
        int databaseSizeBeforeUpdate = tripDayRepository.findAllByActiveTrue().size();

        // Create the TripDay

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTripDayMockMvc.perform(put("/api/trip-days")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripDay)))
            .andExpect(status().isBadRequest());

        // Validate the TripDay in the database
        List<TripDay> tripDayList = tripDayRepository.findAllByActiveTrue();
        assertThat(tripDayList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTripDay() throws Exception {
        // Initialize the database
        tripDayRepository.saveAndFlush(tripDay);

        int databaseSizeBeforeDelete = tripDayRepository.findAllByActiveTrue().size();

        // Delete the tripDay
        restTripDayMockMvc.perform(delete("/api/trip-days/{id}", tripDay.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<TripDay> tripDayList = tripDayRepository.findAllByActiveTrue();
        assertThat(tripDayList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripDay.class);
        TripDay tripDay1 = new TripDay();
        tripDay1.setId(1L);
        TripDay tripDay2 = new TripDay();
        tripDay2.setId(tripDay1.getId());
        assertThat(tripDay1).isEqualTo(tripDay2);
        tripDay2.setId(2L);
        assertThat(tripDay1).isNotEqualTo(tripDay2);
        tripDay1.setId(null);
        assertThat(tripDay1).isNotEqualTo(tripDay2);
    }
}
