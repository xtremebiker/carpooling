package org.xtremebiker.carpooling.web.rest;

import org.xtremebiker.carpooling.CarpoolingApp;
import org.xtremebiker.carpooling.config.TestSecurityConfiguration;
import org.xtremebiker.carpooling.domain.TripGroup;
import org.xtremebiker.carpooling.repository.TripDayRepository;
import org.xtremebiker.carpooling.repository.TripGroupRepository;
import org.xtremebiker.carpooling.service.TripGroupService;
import org.xtremebiker.carpooling.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.xtremebiker.carpooling.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TripGroupResource} REST controller.
 */
@SpringBootTest(classes = {CarpoolingApp.class, TestSecurityConfiguration.class})
public class TripGroupResourceIT {

    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = true;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private TripGroupRepository tripGroupRepository;

    @Autowired
    private TripDayRepository tripDayRepository;

    @Autowired
    private TripGroupService tripGroupService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTripGroupMockMvc;

    private TripGroup tripGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripGroupResource tripGroupResource = new TripGroupResource(tripGroupService, tripDayRepository);
        this.restTripGroupMockMvc = MockMvcBuilders.standaloneSetup(tripGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripGroup createEntity(EntityManager em) {
        TripGroup tripGroup = new TripGroup()
            .groupName(DEFAULT_GROUP_NAME)
            .created(DEFAULT_CREATED)
            .createdBy(DEFAULT_CREATED_BY)
            .updated(DEFAULT_UPDATED)
            .updatedBy(DEFAULT_UPDATED_BY)
            .active(DEFAULT_ACTIVE);
        return tripGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripGroup createUpdatedEntity(EntityManager em) {
        TripGroup tripGroup = new TripGroup()
            .groupName(UPDATED_GROUP_NAME)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);
        return tripGroup;
    }

    @BeforeEach
    public void initTest() {
        tripGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripGroup() throws Exception {
        int databaseSizeBeforeCreate = tripGroupRepository.findAll().size();

        // Create the TripGroup
        restTripGroupMockMvc.perform(post("/api/trip-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripGroup)))
            .andExpect(status().isCreated());

        // Validate the TripGroup in the database
        List<TripGroup> tripGroupList = tripGroupRepository.findAll();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeCreate + 1);
        TripGroup testTripGroup = tripGroupList.get(tripGroupList.size() - 1);
        assertThat(testTripGroup.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testTripGroup.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createTripGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripGroupRepository.findAll().size();

        // Create the TripGroup with an existing ID
        tripGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripGroupMockMvc.perform(post("/api/trip-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripGroup)))
            .andExpect(status().isBadRequest());

        // Validate the TripGroup in the database
        List<TripGroup> tripGroupList = tripGroupRepository.findAll();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkGroupNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = tripGroupRepository.findAll().size();
        // set the field null
        tripGroup.setGroupName(null);

        // Create the TripGroup, which fails.

        restTripGroupMockMvc.perform(post("/api/trip-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripGroup)))
            .andExpect(status().isBadRequest());

        List<TripGroup> tripGroupList = tripGroupRepository.findAll();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTripGroups() throws Exception {
        // Initialize the database
        tripGroupRepository.saveAndFlush(tripGroup);

        // Get all the tripGroupList
        restTripGroupMockMvc.perform(get("/api/trip-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getTripGroup() throws Exception {
        // Initialize the database
        tripGroupRepository.saveAndFlush(tripGroup);

        // Get the tripGroup
        restTripGroupMockMvc.perform(get("/api/trip-groups/{id}", tripGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripGroup.getId().intValue()))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripGroup() throws Exception {
        // Get the tripGroup
        restTripGroupMockMvc.perform(get("/api/trip-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripGroup() throws Exception {
        // Initialize the database
        tripGroupService.save(tripGroup);

        int databaseSizeBeforeUpdate = tripGroupRepository.findAll().size();

        // Update the tripGroup
        TripGroup updatedTripGroup = tripGroupRepository.findById(tripGroup.getId()).get();
        // Disconnect from session so that the updates on updatedTripGroup are not directly saved in db
        em.detach(updatedTripGroup);
        updatedTripGroup
            .groupName(UPDATED_GROUP_NAME)
            .created(UPDATED_CREATED)
            .createdBy(UPDATED_CREATED_BY)
            .updated(UPDATED_UPDATED)
            .updatedBy(UPDATED_UPDATED_BY)
            .active(UPDATED_ACTIVE);

        restTripGroupMockMvc.perform(put("/api/trip-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripGroup)))
            .andExpect(status().isOk());

        // Validate the TripGroup in the database
        List<TripGroup> tripGroupList = tripGroupRepository.findAll();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeUpdate);
        TripGroup testTripGroup = tripGroupList.get(tripGroupList.size() - 1);
        assertThat(testTripGroup.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testTripGroup.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingTripGroup() throws Exception {
        int databaseSizeBeforeUpdate = tripGroupRepository.findAll().size();

        // Create the TripGroup

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTripGroupMockMvc.perform(put("/api/trip-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripGroup)))
            .andExpect(status().isBadRequest());

        // Validate the TripGroup in the database
        List<TripGroup> tripGroupList = tripGroupRepository.findAll();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTripGroup() throws Exception {
        // Initialize the database
        tripGroupService.save(tripGroup);

        int databaseSizeBeforeDelete = tripGroupRepository.findAll().size();

        // Delete the tripGroup
        restTripGroupMockMvc.perform(delete("/api/trip-groups/{id}", tripGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<TripGroup> tripGroupList = tripGroupRepository.findAllByActiveTrue();
        assertThat(tripGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripGroup.class);
        TripGroup tripGroup1 = new TripGroup();
        tripGroup1.setId(1L);
        TripGroup tripGroup2 = new TripGroup();
        tripGroup2.setId(tripGroup1.getId());
        assertThat(tripGroup1).isEqualTo(tripGroup2);
        tripGroup2.setId(2L);
        assertThat(tripGroup1).isNotEqualTo(tripGroup2);
        tripGroup1.setId(null);
        assertThat(tripGroup1).isNotEqualTo(tripGroup2);
    }
}
